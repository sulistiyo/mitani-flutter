import 'package:flutter/material.dart';
import 'package:mitani_flutter/constants.dart';
import 'package:mitani_flutter/home.dart';
import 'package:mitani_flutter/portofolio.dart';
import 'package:mitani_flutter/progres_pengelolaan.dart';
import 'package:mitani_flutter/riwayat_prospektus.dart';
import 'package:text_form_field_wrapper/text_form_field_wrapper.dart';
import 'package:mitani_flutter/detail_lahan.dart';

class ProspektusDana extends StatefulWidget {
  const ProspektusDana({Key? key}) : super(key: key);

  @override
  State<ProspektusDana> createState() => _ProspektusDanaState();
}

class _ProspektusDanaState extends State<ProspektusDana> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: ColorPalette.primaryColor,
          onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Home()));},
        ),
        title: Text(
          'Progres Pengelolaan', style: TextStyle(
          color: Colors.black,
        ),
        ),
        backgroundColor: ColorPalette.wallpaperColor,
      ),
      body: Container(
        color: ColorPalette.wallpaperColor,
        padding: EdgeInsets.all(16),
        child: ListView(
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  _tabBar(context),
                  _cardImage(context),
                  _titleDescription1(),
                  _textField(),
                  _titleDescription2(),
                  _buildButton1(context),
                  _textDescription(),
                  _buildButton2(context),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget _tabBar(BuildContext context){
  return Container(
    padding: EdgeInsets.only(bottom: 16),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          height: 36,
          width: 95,
          child: TextButton(
            style: TextButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(100),
                ),
                backgroundColor: Colors.white),
            onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const ProgresPengelolaan()));},
            child: Text(
              "Progres", style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w600,
              color: ColorPalette.primaryColor,
            ),
            ),
          ),
        ),
        SizedBox(width: 8),
        Container(
          height: 36,
          width: 95,
          child: TextButton(
            style: TextButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(100),
                ),
                backgroundColor: ColorPalette.secondaryColor),
            onPressed: (){},
            child: Text(
              "Prospektus", style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w600,
              color: ColorPalette.primaryColor,
            ),
            ),
          ),
        ),
      ],
    ),
  );
}

Widget _cardImage (BuildContext context){
  return Container(
    height: 100,
    width: 328,
    child: Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12)),
      child: InkWell(
        onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const DetailLahan()));},
        splashColor: Colors.orange,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(padding: EdgeInsets.only(left: 16)),
              ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: Image.asset("images/img_sawah1.jpg",
                  height: 60,
                  width: 60,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(width: 16),
              Container(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Lahan Baru Bagus",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          )),
                      SizedBox(height: 4),
                      Text("Kartonyono, Ngawi",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w300
                          )),
                      SizedBox(height: 4),
                      Text("200 x 100 m",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          )),
                    ],
                  ),
                ),
              ),
              SizedBox(width: 80),
              Icon(Icons.arrow_forward_ios, size: 15, color: Colors.grey),
            ],
          ),
        ),
      ),
    ),
  );
}

Widget _titleDescription1(){
  return Container(
    margin: EdgeInsets.only(right: 16, top: 16, bottom: 16),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text('Input Dokumen Prospektus', style: TextStyle(
          color: Colors.black,
          fontSize: 14,
          fontWeight: FontWeight.w700,
        ),
        ),
      ],
    ),
  );
}

Widget _textField(){
  TextFormField formField = TextFormField(
    initialValue: '',
    decoration: const InputDecoration(border: InputBorder.none),
  );
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(top: 0),
      ),
      SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Container(
              constraints: const BoxConstraints(maxWidth: 600),
              padding: const EdgeInsets.symmetric(
                horizontal: 0,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(
                    padding: EdgeInsets.only(left: 0),
                    child: Text('Kebutuhan Dana', style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),),
                  ),
                  SizedBox(height: 8),
                  TextFormFieldWrapper(
                    formField: TextFormField(
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Masukkan Jumlah Dana',
                        hintStyle: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: ColorPalette.hintColor,
                        ),
                      ),
                    ),
                    position: TextFormFieldPosition.alone,
                  ),
                  const  SizedBox(height: 16),
                  const Padding(
                    padding: EdgeInsets.only(left: 0),
                    child: Text('Deskripsi', style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),),
                  ),
                  SizedBox(height: 8),
                  TextFormFieldWrapper(
                    formField: TextFormField(
                      decoration: const InputDecoration(
                        contentPadding: const EdgeInsets.symmetric(vertical: 32),
                        border: InputBorder.none,
                        hintText: 'Masukkan Deskripsi',
                        hintStyle: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: ColorPalette.hintColor,
                        ),
                      ),
                    ),
                    position: TextFormFieldPosition.alone,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    ],
  );
}

Widget _titleDescription2(){
  return Container(
    margin: EdgeInsets.only(left: 0, right: 16, top: 16, bottom: 4),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text('Dokumen Prospektus', style: TextStyle(
          color: Colors.black,
          fontSize: 14,
          fontWeight: FontWeight.w500,
        ),
        ),
      ],
    ),
  );
}

Widget _buildButton1(BuildContext context){
  return Container(
    width: 328,
    height: 48,
    child: TextButton(
      style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          backgroundColor: ColorPalette.secondaryColor),
      onPressed: (){},
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Upload Dokumen", style: TextStyle(
            color: ColorPalette.primaryColor,
            fontSize: 14,
            fontWeight: FontWeight.w400,
          ),
          ),
          SizedBox(width: 4),
          Icon(Icons.upload_sharp, size: 16, color: ColorPalette.primaryColor),
        ],
      ),
    ),
  );
}

Widget _textDescription(){
  return Container(
    margin: EdgeInsets.only(left: 0, right: 16, top: 4, bottom: 0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text('Ukuran maks. 5MB dengan format PDF, JPG, atau PNG.', style: TextStyle(
          color: Colors.grey,
          fontSize: 10,
          fontWeight: FontWeight.w500,
        ),
        ),
      ],
    ),
  );
}

Widget _buildButton2(BuildContext context){
  return Container(
    margin: EdgeInsets.only(top: 72, bottom: 8),
    width: 328,
    height: 48,
    child: TextButton(
      style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
          backgroundColor: ColorPalette.primaryColor),
      onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const RiwayatProspektus()));},
      child:
      Text(
        "Simpan Progres", style: TextStyle(
        color: ColorPalette.secondaryColor,
        fontSize: 14,
        fontWeight: FontWeight.w600,
      ),
      ),
    ),
  );
}

