import 'package:flutter/material.dart';

class ColorPalette{
  static const primaryColor = Color(0xFF1565C0);
  static const secondaryColor = Color(0xFFBBDEFB);
  static const redColor = Color(0xFFD50000);
  static const pinkColor = Color(0xFFFCE4EC);
  static const greenColor = Color(0xFF00C853);
  static const orangeColor = Color(0xFFFFAB00);
  static const greyColor = Color(0xFF607d8b);
  static const lightblueColor = Color(0xFF448AFF);
  static const borderLineTextField = Color(0xFF607D8B);
  static const underLineTextField = Color(0xFF283593);
  static const hintColor = Color(0xFFB0BEC5);
  static const notifColor = Color(0xFFE3F2FD);
  static const wallpaperColor = Color(0xFFECEFF1);
}