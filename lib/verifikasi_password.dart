import 'package:flutter/material.dart';
import 'package:mitani_flutter/constants.dart';
import 'package:flutter_otp_text_field/flutter_otp_text_field.dart';
import 'package:mitani_flutter/reset_password.dart';

class VerfikasiPassword extends StatefulWidget {
  const VerfikasiPassword({Key? key}) : super(key: key);

  @override
  State<VerfikasiPassword> createState() => _VerfikasiPasswordState();
}

class _VerfikasiPasswordState extends State<VerfikasiPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.all(16),
        child: ListView(
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  _imageTitle(),
                  _titleDescription(),
                  _textFieldOTP(context),
                  _buildButton(context),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

Widget _imageTitle(){
  return Container(
    margin: EdgeInsets.only(top: 24, bottom: 24),
    child: Image.asset('images/img_gembok.png',
      fit: BoxFit.cover,
      height: 100,
    ),
  );
}

Widget _titleDescription(){
  return Container(
    margin: EdgeInsets.only(top: 0),
    child: Column(
      children: [
        Text('Masukkan Kode Verifikasi Kamu', style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w700,
        ),),
        SizedBox(height: 8),
        Text('Masukkan kode verifikasi Kamu, yang kami kirimkan melalui email yang terdaftar',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w400,
            color: ColorPalette.greyColor
        ),)
      ],
    ),
  );
}

Widget _textFieldOTP(BuildContext context){
  return Container(
    margin: EdgeInsets.only(top: 30),
    child: Column(
      children: [
        OtpTextField(
          numberOfFields: 6,
          borderColor: ColorPalette.greyColor,
          //set to true to show as box or false to show as dash
          showFieldAsBox: true,
          //runs when a code is typed in
          onCodeChanged: (String code) {
            //handle validation or checks here
          },
          //runs when every textfield is filled
          onSubmit: (String verificationCode){
            showDialog(
                context: context,
                builder: (context){
                  return AlertDialog(
                    title: Text("Kode Verifikasi"),
                    content: Text('Kode dimasukkan $verificationCode'),
                  );
                }
            );
          }, // end onSubmit
        ),
        SizedBox(height: 32),
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Belum terima kode verifikasi?', style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500,
              ),),
              SizedBox(width: 12),
              Text('Kirim Ulang', style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w600,
                color: ColorPalette.primaryColor
              ),),
            ],
          ),
        )
      ],
    ),
  );
}

Widget _buildButton(BuildContext context){
  return Container(
    margin: EdgeInsets.only(top: 298, bottom: 8),
    width: 328,
    height: 48,
    child: TextButton(
      style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
          backgroundColor: ColorPalette.secondaryColor),
      onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const ResetPassword()));},
      child:
      Text(
        "Kembali", style: TextStyle(
        color: ColorPalette.primaryColor,
        fontSize: 14,
        fontWeight: FontWeight.w600,
      ),
      ),
    ),
  );
}