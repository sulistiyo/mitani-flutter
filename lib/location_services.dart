import 'package:flutter/material.dart';
import 'package:mitani_flutter/constants.dart';
import 'package:mitani_flutter/reset_password.dart';
import 'package:text_form_field_wrapper/text_form_field_wrapper.dart';
import 'package:mitani_flutter/registrasi.dart';
import 'package:mitani_flutter/home.dart';


class Login2 extends StatefulWidget {
  const Login2({Key? key}) : super(key: key);

  @override
  State<Login2> createState() => _Login2State();
}

class _Login2State extends State<Login2> {

  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: formKey,
        child: Container (
          color: ColorPalette.wallpaperColor,
          padding: EdgeInsets.all(16),
          child: ListView(
            children: <Widget>[
              Container(
                child: Form(
                  child: Column(
                    children: <Widget>[
                      _iconLogin(),
                      _titleDescription(),
                      _textField(context),
                      _buildButton1(context),
                      _buildButton2(context),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Widget _iconLogin() {
  return Container(
    margin: EdgeInsets.only(top: 24),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Image.asset("images/logo_flutter.png",
          height: 42,
          width: 42,
        ),
        Text("Mitani", style: TextStyle(
            color: ColorPalette.primaryColor,
            fontSize: 26,
            fontWeight: FontWeight.w800
        ),),
      ],
    ),
  );
}

Widget _titleDescription(){
  return Container(
    margin: EdgeInsets.only(top: 40),
    child: Column(
      children: [
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text("Halo",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
              ),
              Icon(Icons.waving_hand_sharp, size: 18, color: Colors.orangeAccent),
            ],
          ),
        ),
        SizedBox(height: 8),
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Selamat Datang Kembali",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 8),
        Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                "Silahkan masukkan email/username kamu untuk memulai kembali.",
                style: TextStyle(
                  fontSize: 14,
                  color: ColorPalette.greyColor,
                  fontWeight: FontWeight.w400,
                ),
                textAlign: TextAlign.left,
              ),
            ],
          ),
        ),
      ],
    ),
  );
}

Widget _textField(BuildContext context){

  TextEditingController cUser = TextEditingController();
  TextEditingController cPass = TextEditingController();

  TextFormField formField = TextFormField(
    initialValue: '',
    decoration: const InputDecoration(border: InputBorder.none),
  );

  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(top: 32),
      ),
      SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Container(
              constraints: const BoxConstraints(maxWidth: 600),
              padding: const EdgeInsets.symmetric(
                horizontal: 0,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(
                    padding: EdgeInsets.only(left: 0),
                    child: Text('Email/Username', style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),),
                  ),
                  SizedBox(height: 8),
                  TextFormFieldWrapper(
                    formField: TextFormField(
                      controller: cUser,
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Masukkan alamat email kamu',
                        hintStyle: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: ColorPalette.hintColor,
                        ),
                      ),
                      validator: (value){
                        if(value!.isEmpty){
                          return 'User Masih Kosong';
                        }return null;
                      },
                    ),
                    position: TextFormFieldPosition.alone,
                  ),
                  const  SizedBox(height: 16),
                  const Padding(
                    padding: EdgeInsets.only(left: 0),
                    child: Text('Password', style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),),
                  ),
                  SizedBox(height: 8),
                  TextFormFieldWrapper(
                    formField: TextFormField(
                      controller: cPass,
                      obscureText: true,
                      decoration: const InputDecoration(
                        contentPadding: const EdgeInsets.symmetric(vertical: 0),
                        border: InputBorder.none,
                        hintText: 'Masukkan password kamu',
                        hintStyle: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: ColorPalette.hintColor,
                        ),
                      ),
                      validator: (value){
                        if(value!.isEmpty){
                          return 'Password Masih Kosong';
                        }return null;
                      },
                    ),
                    position: TextFormFieldPosition.alone,
                    suffix: const Icon(Icons.remove_red_eye, color: Colors.grey,),
                  ),
                  SizedBox(height: 0),
                  Container(
                      margin: EdgeInsets.only(left: 208),
                      child: TextButton(
                        onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const ResetPassword()));},
                        child: Text('Lupa Password?',style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: ColorPalette.primaryColor,
                        ),
                          textAlign: TextAlign.center,
                        ),
                      )
                  ),
                  SizedBox(height: 166),
                ],
              ),
            ),
          ),
        ),
      ),
    ],
  );
}

Widget _buildButton1(BuildContext context){

  TextEditingController cUser = TextEditingController();
  TextEditingController cPass = TextEditingController();
  final formKey = GlobalKey<FormState>();

  return Container(
    child: Row(
      children: [
        Container(
          width: 272,
          height: 48,
          child: TextButton(
            style: TextButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(100),
                ),
                backgroundColor: ColorPalette.primaryColor),
            onPressed: (){
              String tUser = 'sulis';
              String tPass = 'sulis';
              if(formKey.currentState!.validate()){
                if (cUser.text == tUser && cPass.text == tPass){
                }else{
                  showDialog(
                      context: context,
                      builder: (context){
                        return AlertDialog(
                          title: Text('Konfirmasi Login'),
                          content: Text('Username dan Password Salah'),
                          actions: [
                            ElevatedButton(
                                onPressed: (){},
                                child: Text('OK')
                            )
                          ],
                        );
                      }
                  );
                }
              }
            },
            child:
            Text(
              "Masuk", style: TextStyle(
              color: Colors.white,
              fontSize: 14,
              fontWeight: FontWeight.w600,
            ),
            ),
          ),
        ),

        const SizedBox(
          width: 8,
        ),

        Container(
          width: 48,
          height: 48,
          child: TextButton(
            style: TextButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(100),
                ),
                backgroundColor: ColorPalette.primaryColor),
            onPressed: (){},
            child:
            Icon(Icons.fingerprint_sharp, size: 23, color: Colors.white,
            ),
          ),
        ),
      ],
    ),
  );
}

Widget _buildButton2(BuildContext context){
  return Container(
    margin: EdgeInsets.only(top: 8, bottom: 8),
    width: 328,
    height: 48,
    child: TextButton(
      style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
          backgroundColor: ColorPalette.secondaryColor),
      onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Registrasi()));},
      child:
      Text(
        "Daftar Sebagai Pengelola Lahan", style: TextStyle(
        color: ColorPalette.primaryColor,
        fontSize: 14,
        fontWeight: FontWeight.w600,
      ),
      ),
    ),
  );
}





