import 'package:flutter/material.dart';
import 'package:mitani_flutter/constants.dart';
import 'package:mitani_flutter/login.dart';

class Profil extends StatefulWidget {
  const Profil({Key? key}) : super(key: key);

  @override
  State<Profil> createState() => _ProfilState();
}

class _ProfilState extends State<Profil> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Profil', style: TextStyle(
          color: Colors.black,
        ),
        ),
        automaticallyImplyLeading: false,
        backgroundColor: ColorPalette.wallpaperColor,
      ),
      body: Container(
        color: ColorPalette.wallpaperColor,
        margin: EdgeInsets.only(top: 0),
        child: ListView(
          physics: ClampingScrollPhysics(),
          children: <Widget>[

            //CARD PERTAMA
            Container(
              height: 136,
              width: 328,
              child: Card(
                elevation: 5,
                margin: const EdgeInsets.only(left: 16, right: 16),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16)),
                child: InkWell(
                  onTap: (){},
                  splashColor: Colors.orange,
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                          child: Column(
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(100),
                                child: Image.asset("images/img_marquez.jpg",
                                  height: 56,
                                  width: 56,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 104,
                          width: 191,
                          margin: EdgeInsets.only(top: 0),
                            child: Column(
                              children: [
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Text("Sulistiyo Aji",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w700
                                          )),
                                    ],
                                  ),
                                ),
                                SizedBox(height: 8),
                                Container(
                                  child: Row(
                                    children: [
                                      Text("Pesanggem",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            color: Colors.grey,
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400,

                                          )),
                                    ],
                                  ),
                                ),
                                SizedBox(height: 8),
                                Container(
                                  child: Row(
                                    children: [
                                      Text("NIK 00000000000000000",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            color: Colors.grey,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w400
                                          )),
                                    ],
                                  ),
                                ),
                                SizedBox(height: 8),
                                Container(
                                  child: Row(
                                    children: [
                                      Container(
                                        width: 106,
                                        height: 26,
                                        child: TextButton(
                                          style: TextButton.styleFrom(
                                              shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(8),
                                              ),
                                              backgroundColor: ColorPalette.pinkColor),
                                          onPressed: (){},
                                          child: Text(
                                            "Belum Terverifikasi", style: TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.w500,
                                            color: ColorPalette.redColor,
                                          ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                        ),
                        SizedBox(width: 8),
                        Container(
                          margin: EdgeInsets.only(top: 16, right: 16),
                            child: Column(
                              children: [
                                Icon(
                                    Icons.edit, size: 24, color: Colors.grey
                                ),
                              ],
                            ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),

            //CARD KEDUA
            Container(
              margin: EdgeInsets.only(top: 24),
              height: 72,
              width: 328,
              child: Card(
                color: ColorPalette.notifColor,
                elevation: 5,
                margin: const EdgeInsets.only(left: 16, right: 16),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16)),
                child: InkWell(
                  onTap: (){},
                  splashColor: Colors.orange,
                  child: Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 14, bottom: 14),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Column(
                            children: [
                              Container(
                                  child: Row(
                                    children: [
                                      Text('Menunggu Verifikasi Verifikator',
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w700,
                                      ),
                                      textAlign: TextAlign.left),
                                    ],
                                  )
                              ),
                              SizedBox(height: 8),
                              Container(
                                child: Row(
                                  children: [
                                    Container(
                                        child: Text('1/3', style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400
                                    ),)),
                                    SizedBox(width: 8),
                                    Container(
                                      color: ColorPalette.primaryColor,
                                      height: 4,
                                      width: 70,
                                    ),
                                    SizedBox(width: 8),
                                    Container(
                                      color: Colors.white,
                                      height: 4,
                                      width: 70,
                                    ),
                                    SizedBox(width: 8),
                                    Container(
                                      color: Colors.white,
                                      height: 4,
                                      width: 70,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          )
                        ),
                        Icon(Icons.arrow_forward_ios, size: 16, color: ColorPalette.primaryColor),
                      ],
                    ),
                  ),
                ),
              ),
            ),

            //CARD KETIGA
            Container(
              margin: EdgeInsets.only(top: 24),
              child: Column(
                children: [
                  Card(
                    elevation: 5,
                    margin: const EdgeInsets.only(left: 16, right: 16, top: 0, bottom: 0),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0)),
                    child: InkWell(
                      onTap: (){},
                      splashColor: Colors.orange,
                      child: Container (
                        height: 89,
                        width: 328,
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 16, right: 16),
                              width: 40,
                              height: 40,
                              child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Icon(Icons.landscape_sharp, size: 20, color: Colors.orange),
                                color: Colors.orange.shade50,
                              ),
                            ),
                            Container(
                              height: 56,
                              width: 204,
                              margin: EdgeInsets.only(top: 0),
                              child: Column(
                                children: [
                                  Container(
                                    child: Row(
                                      children: [
                                        Text("Pengelolaan Lahan",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w700
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(height: 4),
                                  Container(
                                    child: Text("Untuk melihat lahan yang sedang kamu kelola",
                                        maxLines: 2,
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w300
                                        )),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(width: 20),
                            Icon(Icons.arrow_forward_ios, size: 16, color: ColorPalette.primaryColor),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

            //CARD KEEMPAT
            Container(
              child: Column(
                children: [
                  Card(
                    elevation: 5,
                    margin: const EdgeInsets.only(left: 16, right: 16, top: 0, bottom: 0),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0)),
                    child: InkWell(
                      onTap: (){},
                      splashColor: Colors.orange,
                      child: Container (
                        height: 89,
                        width: 328,
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 16, right: 16),
                              width: 40,
                              height: 40,
                              child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Icon(Icons.agriculture_sharp, size: 20, color: Colors.green),
                                color: Colors.green.shade50,
                              ),
                            ),
                            Container(
                              height: 56,
                              width: 204,
                              margin: EdgeInsets.only(top: 0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    child: Row(
                                      children: [
                                        Text("Penggunaan Saprotan",
                                            textAlign: TextAlign.left,
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.w700
                                            )),
                                      ],
                                    ),
                                  ),
                                  SizedBox(height: 4),
                                  Container(
                                    child: Text("Untuk melihat saprotan yang sedang kamu gunakan",
                                        maxLines: 2,
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w300
                                        )),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(width: 20),
                            Icon(Icons.arrow_forward_ios, size: 16, color: ColorPalette.primaryColor),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

            //CARD KELIMA
            Container(
              margin: EdgeInsets.only(top: 24),
              height: 58,
              child: Card(
                elevation: 5,
                margin: const EdgeInsets.only(left: 16, right: 16, top: 0, bottom: 0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0)),
                child: InkWell(
                  onTap: (){},
                  splashColor: Colors.orange,
                  child: Container(
                    margin: EdgeInsets.only(left: 16, right: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Center(
                            child: Row(
                              children: [
                                Icon(Icons.settings, size: 20, color: Colors.grey),
                                SizedBox(width: 16),
                                Text("Pengaturan", style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w700
                                )),
                              ],
                            ),
                          ),
                        ),
                        Icon(Icons.arrow_forward_ios, size: 16, color: ColorPalette.primaryColor),
                      ],
                    ),
                  ),
                ),
              ),
            ),

            //CARD KEENAM
            Container(
              height: 58,
              child: Card(
                elevation: 5,
                margin: const EdgeInsets.only(left: 16, right: 16, top: 0, bottom: 0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0)),
                child: InkWell(
                  onTap: (){},
                  splashColor: Colors.orange,
                  child: Container(
                    margin: EdgeInsets.only(left: 16, right: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Center(
                            child: Row(
                              children: [
                                Icon(Icons.help, size: 20, color: Colors.grey),
                                SizedBox(width: 16),
                                Text("Bantuan", style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w700
                                )),
                              ],
                            ),
                          ),
                        ),
                        Icon(Icons.arrow_forward_ios, size: 16, color: ColorPalette.primaryColor),
                      ],
                    ),
                  ),
                ),
              ),
            ),

            Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 24),
              width: 328,
              height: 48,
              child: TextButton(
                style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(100),
                    ),
                    backgroundColor: ColorPalette.pinkColor),
                onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Login()));},
                child:
                Text(
                  "Log Out", style: TextStyle(
                  color: ColorPalette.redColor,
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                ),
                ),
              ),
            ),

            Container(
              margin: EdgeInsets.only(top: 16),
              child: Center(
                child: Text(
                  "Version 1.0"
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
