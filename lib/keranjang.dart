import 'package:flutter/material.dart';
import 'package:mitani_flutter/buat_pesanan.dart';
import 'package:mitani_flutter/constants.dart';

class Keranjang extends StatefulWidget {
  const Keranjang({Key? key}) : super(key: key);

  @override
  State<Keranjang> createState() => _KeranjangState();
}

class _KeranjangState extends State<Keranjang> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: ColorPalette.primaryColor,
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          'Keranjang', style: TextStyle(
          color: Colors.black,
        ),
        ),
        backgroundColor: ColorPalette.wallpaperColor,
      ),
      body: Container(
        color: ColorPalette.wallpaperColor,
        margin: EdgeInsets.only(top: 0),
        child: ListView(
          physics: ClampingScrollPhysics(),
          children: <Widget>[

            //CARD PERTAMA
            Container(
              width: 328,
              child: Card(
                elevation: 5,
                margin: const EdgeInsets.only(left: 16, right: 16, top: 16),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16)),
                child: InkWell(
                  onTap: (){},
                  splashColor: Colors.orange,
                  child: Container(
                    child: Column(
                      children: [
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 16, right: 16, top: 16),
                                child: Column(
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(8),
                                      child: Image.asset("images/img_traktor_kubota.jpg",
                                        height: 102,
                                        width: 102,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                width: 184,
                                margin: EdgeInsets.only(top: 0),
                                child: Column(
                                  children: [
                                    Container(
                                      child: Row(
                                        children: <Widget>[
                                          Icon(Icons.storefront_rounded, size: 13, color: ColorPalette.primaryColor),
                                          SizedBox(width: 4),
                                          Text("Traktor Mang Shinta",
                                              style: TextStyle(
                                                  fontSize: 10,
                                                  color: ColorPalette.primaryColor,
                                                  fontWeight: FontWeight.w700,
                                              )),
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 4),
                                    Container(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text("Traktor Merk Kubota",
                                              style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500,
                                              )),
                                          Icon(Icons.delete_forever_sharp, size: 24, color: ColorPalette.greyColor,)
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 4),
                                    Container(
                                      child: Row(
                                        children: [
                                          Text("Rp. 24.000.000",
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w700
                                              )),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 16, right: 16, top: 12),
                          child: Row(
                            children: [
                              Icon(Icons.note_add_sharp, size: 18, color: ColorPalette.primaryColor),
                              SizedBox(width: 8),
                              Text('Tambah Catatan', style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                color: ColorPalette.primaryColor
                              ),),
                              SizedBox(width: 60),
                              Container(
                                width: 28,
                                height: 28,
                                child: TextButton(
                                  style: TextButton.styleFrom(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(100),
                                      ),
                                      backgroundColor: ColorPalette.primaryColor),
                                  onPressed: (){},
                                  child: Text(
                                    "-", style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white,
                                  ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 44),
                              Container(
                                width: 28,
                                height: 28,
                                child: TextButton(
                                  style: TextButton.styleFrom(
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(100),
                                      ),
                                      backgroundColor: ColorPalette.primaryColor),
                                  onPressed: (){},
                                  child: Text(
                                    "+", style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white,
                                  ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 16),
                      ],
                    ),
                  ),
                ),
              ),
            ),

          ],
        ),
      ),

      bottomNavigationBar: BottomAppBar(
        color: ColorPalette.wallpaperColor,
        child: Padding(
          padding: EdgeInsets.only(left: 16, top: 16, right: 16, bottom: 24),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 168,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      children: [
                        Text('Total Tagihan',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                          ),),
                      ],
                    ),
                    SizedBox(height: 4),
                    Row(
                      children: [
                        Text('Rp. 24.000.000', style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                        ),),
                        Icon(Icons.expand_less_sharp, size: 24,),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                width: 117,
                height: 48,
                child: TextButton(
                  style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(100),
                      ),
                      backgroundColor: ColorPalette.primaryColor),
                  onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const BuatPesanan()));},
                  child: Text(
                    "Checkout", style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
