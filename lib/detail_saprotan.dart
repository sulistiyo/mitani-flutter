import 'package:flutter/material.dart';
import 'package:mitani_flutter/buat_pesanan.dart';
import 'package:mitani_flutter/constants.dart';
import 'package:text_form_field_wrapper/text_form_field_wrapper.dart';
import 'package:mitani_flutter/saprotan.dart';
import 'package:mitani_flutter/keranjang.dart';

class DetailSaprotan extends StatefulWidget {
  const DetailSaprotan({Key? key}) : super(key: key);

  @override
  State<DetailSaprotan> createState() => _DetailSaprotanState();
}

class _DetailSaprotanState extends State<DetailSaprotan> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: ColorPalette.primaryColor,
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          'Detail Saprotan', style: TextStyle(
          color: Colors.black,
        ),
        ),
        backgroundColor: ColorPalette.wallpaperColor,
      ),
      body: Container(
        color: ColorPalette.wallpaperColor,
        padding: EdgeInsets.all(16),
        child: ListView(
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  _cardImage(),
                  _titleDescription(),
                  _buildButton1(context),
                  _buildButton2(context),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget _cardImage (){
  return Container(
    height: 100,
    width: 328,
    child: Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12)),
      child: InkWell(
        onTap: (){},
        splashColor: Colors.orange,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(padding: EdgeInsets.only(left: 16)),
              ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: Image.asset("images/img_traktor_kubota.jpg",
                  height: 60,
                  width: 60,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(width: 16),
              Container(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Traktor Merk Kubota",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          )),
                      SizedBox(height: 4),
                      Text("Sukamekar, Cirebon",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w300
                          )),
                      SizedBox(height: 4),
                      Text("Ro. 2.400.000",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          )),
                    ],
                  ),
                ),
              ),
              SizedBox(width: 74),
              Icon(Icons.arrow_forward_ios, size: 15, color: Colors.grey),
            ],
          ),
        ),
      ),
    ),
  );
}

Widget _titleDescription(){
  return Container(
    margin: EdgeInsets.only(top: 16),
    child: Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4),
      ),
      child: InkWell(
        child: Container(
          margin: EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Deskripsi', style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.w700,
                    ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 8),
              Text('Di segmen alat atau mesin pertanian, salah satu merk yang sudah cukup kondang di pasaran dalam negeri adalah Kubota. Telah berumur lebih dari satu abad, produk andalan perusahaan yang berbasis di Osaka, Jepang ini adalah traktor yang digunakan untuk membajak sawah atau mengolah lahan perkebunan. Tersedia dalam model roda dua dan roda empat. Untuk pertanian, traktor dikatakan memiliki banyak manfaat bagi petani. Salah satu keunggulan penggunaan traktor adalah dapat melakukan substitusi penggunaan tenaga kerja yang kian hari kian mahal dan langka, terutama untuk kegiatan persiapan lahan.', textAlign: TextAlign.justify, style: TextStyle(
                color: Colors.grey,
                fontSize: 14,
                fontWeight: FontWeight.w400,
              ),
              ),
            ],
          ),
        ),
      ),
    ),
  );
}

Widget _buildButton1(BuildContext context){
  return Container(
    margin: EdgeInsets.only(top: 313, bottom: 0),
    width: 328,
    height: 48,
    child: TextButton(
      style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
          backgroundColor: ColorPalette.primaryColor),
      onPressed: (){
        showModalBottomSheet(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20)
          ),
          context: context,
          builder: (context) => Container(
              height: 288,
              padding: EdgeInsets.all(24),
              child: ListView(
                children: [
                  Column(
                    children: [
                      Text('Yakin Pesan Saprotan Ini?', style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w700
                      ),),
                      SizedBox(height: 34),
                      Icon(Icons.agriculture_sharp, size: 100, color: Colors.grey),
                      SizedBox(height: 34),
                      Container(
                        width: 312,
                        height: 48,
                        child: TextButton(
                          style: TextButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(100),
                              ),
                              backgroundColor: ColorPalette.primaryColor),
                          onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const BuatPesanan()));},
                          child: Text(
                            "Ya, Lanjutkan Pesanan", style: TextStyle(
                            color: ColorPalette.secondaryColor,
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          ),
                          ),
                        ),
                      ),

                    ],
                  ),
                ],
              )
          ),
        );
      },
      child: Text(
        "Pesan Sekarang", style: TextStyle(
        color: ColorPalette.secondaryColor,
        fontSize: 14,
        fontWeight: FontWeight.w600,
      ),
      ),
    ),
  );
}

Widget _buildButton2(BuildContext context){
  return Container(
    margin: EdgeInsets.only(top: 8, bottom: 8),
    width: 328,
    height: 48,
    child: TextButton(
      style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
          backgroundColor: ColorPalette.secondaryColor),
      onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Keranjang()));},
      child:
      Text(
        "Tambahkan Ke Keranjang", style: TextStyle(
        color: ColorPalette.primaryColor,
        fontSize: 14,
        fontWeight: FontWeight.w600,
      ),
      ),
    ),
  );
}


