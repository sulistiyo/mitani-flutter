import 'package:flutter/material.dart';
import 'package:mitani_flutter/constants.dart';
import 'package:mitani_flutter/detail_lahan.dart';
import 'package:mitani_flutter/notifikasi.dart';
import 'package:text_form_field_wrapper/text_form_field_wrapper.dart';
import 'package:mitani_flutter/lahan.dart';
import 'package:mitani_flutter/home.dart';
import 'package:mitani_flutter/detail_lahan.dart';

class KontrakLahan extends StatefulWidget {
  const KontrakLahan({Key? key}) : super(key: key);

  @override
  State<KontrakLahan> createState() => _KontrakLahanState();
}

class _KontrakLahanState extends State<KontrakLahan> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: ColorPalette.primaryColor,
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
            'Ajukan Kontrak', style: TextStyle(
          color: Colors.black,
        ),
        ),
        backgroundColor: ColorPalette.wallpaperColor,
      ),
      body: Container(
        color: ColorPalette.wallpaperColor,
        padding: EdgeInsets.all(16),
        child: ListView(
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  _cardImage(context),
                  _titleDescription(),
                  _textField(),
                  _buildButton1(context),
                  _buildButton2(context),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget _cardImage (BuildContext context){
  return Container(
    height: 100,
    width: 328,
    child: Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12)),
      child: InkWell(
        onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const DetailLahan()));},
        splashColor: Colors.orange,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(padding: EdgeInsets.only(left: 16)),
              ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: Image.asset("images/img_sawah1.jpg",
                  height: 60,
                  width: 60,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(width: 16),
              Container(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Lahan Baru Bagus",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          )),
                      SizedBox(height: 4),
                      Text("Kartonyono, Ngawi",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w300
                          )),
                      SizedBox(height: 4),
                      Text("200 x 100 m",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          )),
                    ],
                  ),
                ),
              ),
              SizedBox(width: 80),
              Icon(Icons.arrow_forward_ios, size: 15, color: Colors.grey),
            ],
          ),
        ),
      ),
    ),
  );
}

Widget _titleDescription(){
  return Container(
    margin: EdgeInsets.only(right: 16, top: 16, bottom: 16),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text('Detail Kontrak', style: TextStyle(
          color: Colors.black,
          fontSize: 14,
          fontWeight: FontWeight.w700,
        ),
        ),
      ],
    ),
  );
}

Widget _textField(){
  TextFormField formField = TextFormField(
    initialValue: '',
    decoration: const InputDecoration(border: InputBorder.none),
  );
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(top: 0),
      ),
      SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Container(
              constraints: const BoxConstraints(maxWidth: 600),
              padding: const EdgeInsets.symmetric(
                horizontal: 0,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(
                    padding: EdgeInsets.only(left: 0),
                    child: Text('Jangka Kontrak', style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),),
                  ),
                  SizedBox(height: 8),
                  TextFormFieldWrapper(
                    formField: TextFormField(
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Jangka kontrak dalam bulan',
                        hintStyle: TextStyle(
                          fontSize: 16,
                          color: ColorPalette.hintColor,
                        ),
                      ),
                    ),
                    position: TextFormFieldPosition.alone,
                  ),
                  const  SizedBox(height: 16),
                  const Padding(
                    padding: EdgeInsets.only(left: 0),
                    child: Text('Penggunaan Lahan', style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),),
                  ),
                  SizedBox(height: 8),
                  TextFormFieldWrapper(
                    formField: TextFormField(
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Masukkan penggunaan lahan',
                        hintStyle: TextStyle(
                          fontSize: 16,
                          color: ColorPalette.hintColor,
                        ),
                      ),
                    ),
                    position: TextFormFieldPosition.alone,
                  ),
                  const  SizedBox(height: 16),
                  const Padding(
                    padding: EdgeInsets.only(left: 0),
                    child: Text('Sistem Bagi Hasil', style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),),
                  ),
                  SizedBox(height: 8),
                  TextFormFieldWrapper(
                    formField: TextFormField(
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Sistem bagi hasil yang ditawarkan',
                        hintStyle: TextStyle(
                          fontSize: 16,
                          color: ColorPalette.hintColor,
                        ),
                      ),
                    ),
                    position: TextFormFieldPosition.alone,
                  ),
                  SizedBox(height: 136),
                ],
              ),
            ),
          ),
        ),
      ),
    ],
  );
}

Widget _buildButton1(BuildContext context){
  return Container(
    margin: EdgeInsets.only(top: 0, bottom: 0),
    width: 328,
    height: 48,
    child: TextButton(
      style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
          backgroundColor: ColorPalette.primaryColor),
      onPressed: (){
        showModalBottomSheet(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20)
          ),
            context: context,
            builder: (context) => Container(
              height: 452,
              padding: EdgeInsets.all(24),
              child: ListView(
                children: [
                  Column(
                    children: [
                      Container(
                        child: Row(
                          children: [
                            Text('Detail Kontrak', style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w700
                            ),),
                          ],
                        ),
                      ),
                      SizedBox(height: 24),
                      Container(
                        padding: EdgeInsets.only(top: 12, bottom: 12),
                        decoration: BoxDecoration(
                          border: Border(
                            top: BorderSide(
                              color: ColorPalette.greyColor,
                              width: 1,
                            ),
                            bottom: BorderSide(
                              color: ColorPalette.greyColor,
                              width: 1,
                            ),
                          ),
                        ),
                        child: Container(
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Text('Lama Kontrak', style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400
                                  ),),
                                ],
                              ),
                              SizedBox(height: 8),
                              Row(
                                children: [
                                  Text('7 Bulan', style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500
                                  ),),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 12, bottom: 12),
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              color: ColorPalette.greyColor,
                              width: 1,
                            ),
                          ),
                        ),
                        child: Container(
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Text('Penggunaan Lahan', style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400
                                  ),),
                                ],
                              ),
                              SizedBox(height: 8),
                              Row(
                                children: [
                                  Text('Penanaman Jagung', style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500
                                  ),),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 12, bottom: 12),
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              color: ColorPalette.greyColor,
                              width: 1,
                            ),
                          ),
                        ),
                        child: Container(
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Text('Sistem Bagi Hasil', style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                  ),
                                    textAlign: TextAlign.left,),
                                ],
                              ),
                              SizedBox(height: 8),
                              Row(
                                children: [
                                  Text('5% untuk pemilik tanah', style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500
                                  ),),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),

                      Container(
                        margin: EdgeInsets.only(top: 32, bottom: 0),
                        width: 312,
                        height: 48,
                        child: TextButton(
                          style: TextButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(100),
                              ),
                              backgroundColor: ColorPalette.primaryColor),
                          onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Notifikasi()));},
                          child:
                          Text(
                            "Ajukan Kontrak", style: TextStyle(
                            color: ColorPalette.secondaryColor,
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          ),
                          ),
                        ),
                      ),

                      Container(
                        margin: EdgeInsets.only(top: 8, bottom: 0),
                        width: 312,
                        height: 48,
                        child: TextButton(
                          style: TextButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(100),
                              ),
                              backgroundColor: ColorPalette.secondaryColor),
                          onPressed: (){},
                          child:
                          Text(
                            "Simpan Sebagai Draft", style: TextStyle(
                            color: ColorPalette.primaryColor,
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          ),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              )
            ),
        );
      },
      child: Text(
        "Mulai Kontrak", style: TextStyle(
        color: ColorPalette.secondaryColor,
        fontSize: 14,
        fontWeight: FontWeight.w600,
      ),
      ),
    ),
  );
}

Widget _buildButton2(BuildContext context){
  return Container(
    margin: EdgeInsets.only(top: 8, bottom: 8),
    width: 328,
    height: 48,
    child: TextButton(
      style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
          backgroundColor: ColorPalette.secondaryColor),
      onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Home()));},
      child:
      Text(
        "Lihat Lahan Lainnya", style: TextStyle(
        color: ColorPalette.primaryColor,
        fontSize: 14,
        fontWeight: FontWeight.w600,
      ),
      ),
    ),
  );
}


