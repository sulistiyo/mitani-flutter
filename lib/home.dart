import 'package:flutter/material.dart';
import 'package:mitani_flutter/constants.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:mitani_flutter/homepage.dart';
import 'package:mitani_flutter/lahan.dart';
import 'package:mitani_flutter/profil.dart';
import 'package:mitani_flutter/portofolio.dart';
import 'package:mitani_flutter/saprotan.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  // CARD CARAUSELCONTROLLER
  int _current = 0;
  final CarouselController _controller = CarouselController();
  final List<Widget> myData = [
    Container(
      height: 199,
      width: 344,
      color: Colors.red,
      child: Center(child: Text('Picture 1')
      ),
    ),
    Container(
      height: 199,
      width: 344,
      color: Colors.yellow,
      child: Center(child: Text('Picture 2')
      ),
    ),
    Container(
      height: 199,
      width: 344,
      color: Colors.green,
      child: Center(child: Text('Picture 3')
      ),
    ),
    Container(
      height: 199,
      width: 344,
      color: Colors.blue,
      child: Center(child: Text('Picture 4')
      ),
    ),
  ];

  //BOTTOM NAVIGATION
  late int index;
  List showWidget = [
    //Center(child: Text("Ini Halaman Home")),
    HomePage(),
    //Center(child: Text("Ini Halaman Lahan")),
    Lahan(),
    //Center(child: Text("Ini Halaman Saprotan")),
    Saprotan(),
    //Center(child: Text("Ini Halaman Portofolio")),
    Portofolio(),
    //Center(child: Text("Ini halaman Profil")),
    Profil(),
  ];

  @override
  void initState(){
    index = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: showWidget[index],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: ColorPalette.wallpaperColor,
        selectedItemColor: ColorPalette.primaryColor,
        unselectedItemColor: Colors.grey,
        iconSize: 24,
        currentIndex: index,
        onTap: (value){setState(() {
          index = value;
        });},
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home_sharp),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.landscape_sharp),
            label: 'Lahan',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.agriculture_sharp),
            label: 'Saprotan',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.pie_chart_sharp),
            label: 'Portofolio',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.supervisor_account_sharp),
            label: 'Profil',
          ),
        ],
      ),
    );
  }
}
