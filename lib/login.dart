import 'package:flutter/material.dart';
import 'package:mitani_flutter/constants.dart';
import 'package:mitani_flutter/reset_password.dart';
import 'package:text_form_field_wrapper/text_form_field_wrapper.dart';
import 'package:mitani_flutter/registrasi.dart';
import 'package:mitani_flutter/home.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {

  TextFormField formField = TextFormField(
    initialValue: '',
    decoration: const InputDecoration(border: InputBorder.none),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        child: Container(
          color: ColorPalette.wallpaperColor,
          padding: EdgeInsets.all(16),
          child: ListView(
            physics: ClampingScrollPhysics(),
            children: <Widget>[

              //ICON LOGIN
              Container(
                margin: EdgeInsets.only(top: 24),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Image.asset("images/logo_flutter.png",
                      height: 42,
                      width: 42,
                    ),
                    Text("Mitani", style: TextStyle(
                        color: ColorPalette.primaryColor,
                        fontSize: 26,
                        fontWeight: FontWeight.w800
                    ),),
                  ],
                ),
              ),

              //TITLE DESCRIPTION
              Container(
                margin: EdgeInsets.only(top: 40),
                child: Column(
                  children: [
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text("Halo",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Icon(Icons.waving_hand_sharp, size: 18, color: Colors.orangeAccent),
                        ],
                      ),
                    ),
                    SizedBox(height: 8),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Selamat Datang Kembali",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 8),
                    Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            "Silahkan masukkan email/username kamu untuk memulai kembali.",
                            style: TextStyle(
                              fontSize: 14,
                              color: ColorPalette.greyColor,
                              fontWeight: FontWeight.w400,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),

              //TEXT FIELD
              Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 32),
                  ),
                  SafeArea(
                    child: Center(
                      child: SingleChildScrollView(
                        child: Container(
                          constraints: const BoxConstraints(maxWidth: 600),
                          padding: const EdgeInsets.symmetric(
                            horizontal: 0,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Padding(
                                padding: EdgeInsets.only(left: 0),
                                child: Text('Email/Username', style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500
                                ),),
                              ),
                              SizedBox(height: 8),
                              TextFormFieldWrapper(
                                formField: TextFormField(
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'Masukkan alamat email kamu',
                                    hintStyle: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      color: ColorPalette.hintColor,
                                    ),
                                  ),
                                  validator: (value){
                                    if(value!.isEmpty){
                                      return 'User Masih Kosong';
                                    }return null;
                                  },
                                ),
                                position: TextFormFieldPosition.alone,
                              ),
                              const  SizedBox(height: 16),
                              const Padding(
                                padding: EdgeInsets.only(left: 0),
                                child: Text('Password', style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500
                                ),),
                              ),
                              SizedBox(height: 8),
                              TextFormFieldWrapper(
                                formField: TextFormField(
                                  obscureText: true,
                                  decoration: const InputDecoration(
                                    contentPadding: const EdgeInsets.symmetric(vertical: 0),
                                    border: InputBorder.none,
                                    hintText: 'Masukkan password kamu',
                                    hintStyle: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      color: ColorPalette.hintColor,
                                    ),
                                  ),
                                ),
                                position: TextFormFieldPosition.alone,
                                suffix: const Icon(Icons.remove_red_eye, color: Colors.grey,),
                              ),
                              SizedBox(height: 0),
                              Container(
                                  margin: EdgeInsets.only(left: 208),
                                  child: TextButton(
                                    onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const ResetPassword()));},
                                    child: Text('Lupa Password?',style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                      color: ColorPalette.primaryColor,
                                    ),
                                      textAlign: TextAlign.center,
                                    ),
                                  )
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),

              //BUILD BUTTON 1
              Container(
                margin: EdgeInsets.only(top: 164),
                child: Row(
                  children: [
                    Container(
                      width: 272,
                      height: 48,
                      child: TextButton(
                        style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(100),
                            ),
                            backgroundColor: ColorPalette.primaryColor),
                        onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Home()));},
                        child:
                        Text(
                          "Masuk", style: TextStyle(
                          color: Colors.white,
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                        ),
                        ),
                      ),
                    ),

                    const SizedBox(
                      width: 8,
                    ),

                    Container(
                      width: 48,
                      height: 48,
                      child: TextButton(
                        style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(100),
                            ),
                            backgroundColor: ColorPalette.primaryColor),
                        onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Home()));},
                        child:
                        Icon(Icons.fingerprint_sharp, size: 23, color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              //BUILD BUTTON 2
              Container(
                margin: EdgeInsets.only(top: 8, bottom: 8),
                width: 328,
                height: 48,
                child: TextButton(
                  style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(100),
                      ),
                      backgroundColor: ColorPalette.secondaryColor),
                  onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Registrasi()));},
                  child:
                  Text(
                    "Daftar Sebagai Pengelola Lahan", style: TextStyle(
                    color: ColorPalette.primaryColor,
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                  ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
