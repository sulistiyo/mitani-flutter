import 'package:flutter/material.dart';
import 'package:mitani_flutter/constants.dart';

class Notifikasi extends StatefulWidget {
  const Notifikasi({Key? key}) : super(key: key);

  @override
  State<Notifikasi> createState() => _NotifikasiState();
}

class _NotifikasiState extends State<Notifikasi> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: ColorPalette.primaryColor,
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          'Notifikasi', style: TextStyle(
          color: Colors.black,
        ),
        ),
        backgroundColor: ColorPalette.wallpaperColor,
      ),
      body: Container(
        color: ColorPalette.wallpaperColor,
        margin: EdgeInsets.only(top: 0),
        child: ListView(
          physics: ClampingScrollPhysics(),
          children: <Widget>[

            //CARD PERTAMA
            Container(
              height: 80,
              width: 328,
              child: Card(
                color: ColorPalette.notifColor,
                elevation: 5,
                margin: const EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16)),
                child: InkWell(
                  onTap: (){},
                  splashColor: Colors.orange,
                  child: Container(
                      margin: EdgeInsets.all(0),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 12),
                                child: CircleAvatar(
                                  maxRadius: 18,
                                  backgroundColor: Colors.white,
                                  child: IconButton(
                                    icon: Icon(Icons.landscape_sharp, size: 20),
                                    onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Notifikasi()));},
                                    color: ColorPalette.primaryColor,
                                    splashColor: Colors.orange,
                                    splashRadius: 2,
                                    highlightColor: Colors.red,
                                  ),
                                ),
                              ),

                              SizedBox(width: 8),
                              Container(
                                child: Text(
                                  'Info', style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.grey
                                ),
                                ),
                              ),

                              SizedBox(width: 158),
                              Container(
                                child: Text(
                                  '19.00 WIB', style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.grey
                                ),
                                ),
                              ),
                            ],
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 56, right: 12, bottom: 12),
                              child: Row(
                                children: [
                                  Text(
                                    'Pengajuan Lahan Anda Diterima',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w700
                                    ),
                                  ),
                                ],
                              )),
                        ],
                      )
                  ),
                ),
              ),
            ),

            //CARD KEDUA
            Container(
              height: 80,
              width: 328,
              child: Card(
                color: ColorPalette.notifColor,
                elevation: 5,
                margin: const EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16)),
                child: InkWell(
                  onTap: (){},
                  splashColor: Colors.orange,
                  child: Container(
                      margin: EdgeInsets.all(0),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 12),
                                child: CircleAvatar(
                                  maxRadius: 18,
                                  backgroundColor: Colors.white,
                                  child: IconButton(
                                    icon: Icon(Icons.landscape_sharp, size: 20),
                                    onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Notifikasi()));},
                                    color: ColorPalette.redColor,
                                    splashColor: Colors.orange,
                                    splashRadius: 2,
                                    highlightColor: Colors.red,
                                  ),
                                ),
                              ),

                              SizedBox(width: 8),
                              Container(
                                child: Text(
                                  'Info', style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.grey
                                ),
                                ),
                              ),

                              SizedBox(width: 158),
                              Container(
                                child: Text(
                                  '20.00 WIB', style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.grey
                                ),
                                ),
                              ),
                            ],
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 56, right: 12, bottom: 12),
                              child: Row(
                                children: [
                                  Text(
                                    'Pengajuan Lahan Anda Ditolak',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w700
                                    ),
                                  ),
                                ],
                              )),
                        ],
                      )
                  ),
                ),
              ),
            ),


            //CARD KETIGA
            Container(
              height: 112,
              width: 328,
              child: Card(
                color: ColorPalette.notifColor,
                elevation: 5,
                margin: const EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16)),
                child: InkWell(
                  onTap: (){},
                  splashColor: Colors.orange,
                  child: Container(
                    margin: EdgeInsets.all(0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                                Container(
                                  margin: EdgeInsets.only(left: 12),
                                  child: CircleAvatar(
                                    maxRadius: 18,
                                    backgroundColor: Colors.white,
                                    child: IconButton(
                                      icon: Icon(Icons.info_sharp, size: 20),
                                      onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Notifikasi()));},
                                      color: ColorPalette.primaryColor,
                                      splashColor: Colors.orange,
                                      splashRadius: 2,
                                      highlightColor: Colors.red,
                                    ),
                                  ),
                                ),

                                SizedBox(width: 8),
                                Container(
                                  child: Text(
                                    'Info', style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.grey
                                  ),
                                  ),
                                ),

                                SizedBox(width: 158),
                                Container(
                                  child: Text(
                                    '21.00 WIB', style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.grey
                                  ),
                                  ),
                                ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 56, right: 12, bottom: 12),
                            child: Text(
                                'Verifikasimu diterima. Silakan pilih dan kelola lahan kamu yang ingin kamu kelola',
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w700
                              ),
                            )),
                      ],
                    )
                  ),
                ),
              ),
            ),

            //CARD KEEMPAT
            Container(
              height: 112,
              width: 328,
              child: Card(
                color: Colors.white,
                elevation: 5,
                margin: const EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16)),
                child: InkWell(
                  onTap: (){},
                  splashColor: Colors.orange,
                  child: Container(
                      margin: EdgeInsets.all(0),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 12),
                                child: CircleAvatar(
                                  maxRadius: 18,
                                  backgroundColor: Colors.grey.shade200,
                                  child: IconButton(
                                    icon: Icon(Icons.info_sharp, size: 20),
                                    onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Notifikasi()));},
                                    color: ColorPalette.redColor,
                                    splashColor: Colors.orange,
                                    splashRadius: 2,
                                    highlightColor: Colors.red,
                                  ),
                                ),
                              ),

                              SizedBox(width: 8),
                              Container(
                                child: Text(
                                  'Info', style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.grey
                                ),
                                ),
                              ),

                              SizedBox(width: 158),
                              Container(
                                child: Text(
                                  '22.00 WIB', style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.grey
                                ),
                                ),
                              ),
                            ],
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 56, right: 12, bottom: 12),
                              child: Text(
                                'Verifikasimu ditolak karena tidak lengkap. Silakan cek kembali dokumen kamu',
                                style: TextStyle(
                                  color: Colors.grey,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500
                                ),
                              )),
                        ],
                      )
                  ),
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
