import 'package:flutter/material.dart';
import 'package:mitani_flutter/constants.dart';
import 'package:text_form_field_wrapper/text_form_field_wrapper.dart';

class BuatPesanan extends StatefulWidget {
  const BuatPesanan({Key? key}) : super(key: key);

  @override
  State<BuatPesanan> createState() => _BuatPesananState();
}

class _BuatPesananState extends State<BuatPesanan> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: ColorPalette.primaryColor,
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          'Pesanan', style: TextStyle(
          color: Colors.black,
        ),
        ),
        backgroundColor: ColorPalette.wallpaperColor,
      ),

      body: Container(
        color: ColorPalette.wallpaperColor,
        margin: EdgeInsets.all(0),
        child: ListView(
          physics: ClampingScrollPhysics(),
          children: <Widget>[
            
            Container(
              margin: EdgeInsets.only(left: 16, top: 16),
              child: Text('Produk', style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w700,
              ),),
            ),
            
            Container(
              width: 328,
              height: 166,
              margin: EdgeInsets.all(16),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                color: Colors.white,
              ),
              child: Column(
                children: [
                  Container(
                        child: Column(
                          children:[
                            Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(left: 16, top: 16),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(16),
                                    child: Image.asset("images/img_traktor_kubota.jpg",
                                      height: 64,
                                      width: 64,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                SizedBox(width: 12),
                                Container(
                                  height: 44,
                                  width: 220,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Row(
                                        children: [
                                          Text("Traktor Merk Kubota",
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w500
                                              )),
                                        ],
                                      ),
                                      SizedBox(height: 4),
                                      Row(
                                        children: [
                                          Text("Rp. 24.000.000",
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w700
                                              )),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 20),

                            Container(
                              height: 48,
                              width: 296,
                              child: TextFormFieldWrapper(
                                formField: TextFormField(
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'Tambahkan komentar',
                                    hintStyle: TextStyle(
                                      fontSize: 14,
                                      color: ColorPalette.hintColor,
                                    ),
                                  ),
                                ),
                                position: TextFormFieldPosition.alone,
                              ),
                            ),
                          ],
                        ),
                  ),
                ],
              ),
            ),

            Container(
              margin: EdgeInsets.only(left: 16, right: 16),
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: Colors.white,
                border: Border.all(color: ColorPalette.lightblueColor),
              ),
              child: Row(
                children: [
                  Icon(Icons.info_sharp, size: 24, color: ColorPalette.lightblueColor,),
                  SizedBox(width: 8),
                  Container(
                    width: 262,
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Text('Syarat dan Ketentuan', style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w700
                            ),),
                          ],
                        ),
                        SizedBox(height: 4,),
                        RichText(text: TextSpan(
                          children: [
                            TextSpan(text: 'Dengan melanjutkan transaksi ini kamu dianggap setuju dengan ', style: TextStyle(
                              fontSize: 12,
                              color: ColorPalette.greyColor,
                              fontWeight: FontWeight.w400,
                            )),
                            TextSpan(text: 'syarat dan ketentuan ', style: TextStyle(
                              fontSize: 12,
                              color: ColorPalette.lightblueColor,
                              fontWeight: FontWeight.w400,
                            )),
                            TextSpan(text: 'yang berlaku.', style: TextStyle(
                              fontSize: 12,
                              color: ColorPalette.greyColor,
                              fontWeight: FontWeight.w400,
                            )),
                          ]
                        )),
                      ],
                    ),
                  ),
                ],
              ),
            ),

            Container(
              margin: EdgeInsets.only(left: 16, top: 16),
                child: Text('Informasi Pemesanan', style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),)),

            Container(
              margin: EdgeInsets.only(left: 16, top: 16, right: 16),
              child: Column(
                children: <Widget>[
                  SafeArea(
                    child: Center(
                      child: SingleChildScrollView(
                        child: Container(
                          constraints: const BoxConstraints(maxWidth: 600),
                          padding: const EdgeInsets.symmetric(
                            horizontal: 0,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Padding(
                                padding: EdgeInsets.only(left: 0),
                                child: Text('Nama', style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500
                                ),),
                              ),
                              SizedBox(height: 12),
                              TextFormFieldWrapper(
                                formField: TextFormField(
                                  controller: TextEditingController(text: 'Sulistiyo Aji'),
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                  ),
                                ),
                                position: TextFormFieldPosition.alone,
                              ),
                              const  SizedBox(height: 24),
                              const Padding(
                                padding: EdgeInsets.only(left: 0),
                                child: Text('Nomor Handphone', style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500
                                ),),
                              ),
                              SizedBox(height: 12),
                              TextFormFieldWrapper(
                                formField: TextFormField(
                                  controller: TextEditingController(text: '085777777777'),
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                  ),
                                ),
                                position: TextFormFieldPosition.alone,
                              ),
                              const  SizedBox(height: 24),
                              const Padding(
                                padding: EdgeInsets.only(left: 0),
                                child: Text('Email', style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500
                                ),),
                              ),
                              SizedBox(height: 12),
                              TextFormFieldWrapper(
                                formField: TextFormField(
                                  controller: TextEditingController(text: 'Sul@gmail.com'),
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                  ),
                                ),
                                position: TextFormFieldPosition.alone,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

            Container(
                margin: EdgeInsets.only(left: 16, top: 24, right: 16),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text('Informasi Penerima', style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                        ),),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 16),
                      width: 328,
                      height: 48,
                      child: TextButton(
                        style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(100),
                            ),
                            backgroundColor: ColorPalette.secondaryColor),
                        onPressed: (){},
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.add, size: 18, color: ColorPalette.primaryColor,),
                            SizedBox(width: 8),
                            Text(
                              "Alamat Pengiriman", style: TextStyle(
                              color: ColorPalette.primaryColor,
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                            ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                )),

            Container(
                margin: EdgeInsets.only(left: 16, top: 24, right: 16),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text('Metode Pengiriman', style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                        ),),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 16),
                      width: 328,
                      height: 48,
                      child: TextButton(
                        style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(100),
                            ),
                            backgroundColor: ColorPalette.secondaryColor),
                        onPressed: (){},
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.delivery_dining_sharp, size: 18, color: ColorPalette.primaryColor,),
                            SizedBox(width: 8),
                            Text(
                              "Pilih Metode Pengiriman", style: TextStyle(
                              color: ColorPalette.primaryColor,
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                            ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                )),

            Container(
                margin: EdgeInsets.only(left: 16, top: 24, right: 16),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text('Metode Pembayaran', style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                        ),),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 16),
                      width: 328,
                      height: 48,
                      child: TextButton(
                        style: TextButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(100),
                            ),
                            backgroundColor: ColorPalette.secondaryColor),
                        onPressed: (){},
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.paid_sharp, size: 18, color: ColorPalette.primaryColor,),
                            SizedBox(width: 8),
                            Text(
                              "Pilih Metode Pembayaran", style: TextStyle(
                              color: ColorPalette.primaryColor,
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                            ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                )),

            SizedBox(height: 56),

          ],
        ),
      ),

      bottomNavigationBar: BottomAppBar(
        color: ColorPalette.wallpaperColor,
        child: Padding(
          padding: EdgeInsets.only(left: 16, top: 16, right: 16, bottom: 24),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 168,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      children: [
                        Text('Total Tagihan',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                          ),),
                      ],
                    ),
                    SizedBox(height: 4),
                    Row(
                      children: [
                        Text('Rp. 24.000.000', style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                        ),),
                        Icon(Icons.expand_less_sharp, size: 24,),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                width: 88,
                height: 48,
                child: TextButton(
                  style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(100),
                      ),
                      backgroundColor: ColorPalette.primaryColor),
                  onPressed: (){},
                  child: Text(
                    "Bayar", style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),

    );
  }
}
