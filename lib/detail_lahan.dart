import 'package:flutter/material.dart';
import 'package:mitani_flutter/constants.dart';
import 'package:mitani_flutter/kontrak_lahan.dart';

class DetailLahan extends StatefulWidget {
  const DetailLahan({Key? key}) : super(key: key);

  @override
  State<DetailLahan> createState() => _DetailLahanState();
}

class _DetailLahanState extends State<DetailLahan> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: ColorPalette.primaryColor,
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          'Lahan Baru Bagus', style: TextStyle(
          color: Colors.black,
        ),
        ),
        backgroundColor: ColorPalette.wallpaperColor,
      ),

      body: Container(
        color: ColorPalette.wallpaperColor,
        margin: EdgeInsets.only(top: 0),
        child: ListView(
          physics: ClampingScrollPhysics(),
          children: <Widget>[

            //CARD PERTAMA
            Container(
              child: Image.asset(
                  'images/img_sawah1.jpg',
              height: 220,
              fit: BoxFit.cover
              ),
            ),

            // CARD KEDUA
            Container(
              margin: EdgeInsets.all(0),
              child: Card(
                child: Container(
                  margin: EdgeInsets.all(16),
                  child: Column(
                    children: [
                      Container(
                          child: Row(
                            children: [
                              Text('Luas (m2)', style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                              ),),
                            ],
                          )
                      ),
                      Container(
                          padding: EdgeInsets.only(top: 4),
                          child: Row(
                            children: [
                              Text('200 x 120', style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400
                              ),),
                            ],
                          )
                      ),
                      SizedBox(height: 8),
                      Container(
                          child: Row(
                            children: [
                              Text('Jenis Tanah', style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                              ),),
                            ],
                          )
                      ),
                      Container(
                          padding: EdgeInsets.only(top: 4),
                          child: Row(
                            children: [
                              Text('Kering', style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400
                              ),),
                            ],
                          )
                      ),
                      SizedBox(height: 8),
                      Container(
                          child: Row(
                            children: [
                              Text('Minimal Kontrak Lahan', style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                              ),),
                            ],
                          )
                      ),
                      Container(
                          padding: EdgeInsets.only(top: 4),
                          child: Row(
                            children: [
                              Text('6 Bulan', style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400
                              ),),
                            ],
                          )
                      ),
                    ],
                  ),
                )
              )
            ),

            //CARD KETIGA
            Container(
                margin: EdgeInsets.only(top: 0),
                child: Card(
                    child: Container(
                      margin: EdgeInsets.all(16),
                      child: Column(
                        children: [
                          Container(
                              child: Row(
                                children: [
                                  Text('Lokasi', style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w700,
                                  ),),
                                ],
                              )
                          ),
                          Container(
                              padding: EdgeInsets.only(top: 4),
                              child: Row(
                                children: [
                                  Text('Kartonyono, Ngawi, Jawa Timur, Indonesia', style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400
                                  ),),
                                ],
                              )
                          ),
                          Container(
                            width: 328,
                            padding: EdgeInsets.only(top: 16),
                            child: Image.asset('images/img_maps_ngawi.jpg'),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 16),
                            width: 328,
                            height: 40,
                            child: TextButton(
                              style: TextButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(100),
                                  ),
                                  backgroundColor: ColorPalette.secondaryColor),
                              onPressed: (){},
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(Icons.navigation_sharp, size: 18, color: ColorPalette.primaryColor),
                                  SizedBox(width: 8),
                                  Text(
                                    "Petunjuk Arah", style: TextStyle(
                                      color: ColorPalette.primaryColor,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600
                                  ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                ),
            ),

            //CARD KEEMPAT
            Container(
              margin: EdgeInsets.only(top: 0),
              child: Card(
                child: Container(
                  margin: EdgeInsets.all(16),
                  child: Column(
                    children: [
                      Container(
                        child: Row(
                          children: [
                            Text('Tanaman yang disarankan', style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w700,
                            ),),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Tanaman', style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                            ),),
                            Text('Tingkat Keberhasilan', style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                            ),),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Jagung', style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w700,
                            ),),
                            Text('99,08%', style: TextStyle(
                              color: ColorPalette.greenColor,
                              fontSize: 12,
                              fontWeight: FontWeight.w700,
                            ),),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Sawit', style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w700,
                            ),),
                            Text('84,02%', style: TextStyle(
                              color: ColorPalette.greenColor,
                              fontSize: 12,
                              fontWeight: FontWeight.w700,
                            ),),
                          ],
                        ),
                      ),Container(
                        margin: EdgeInsets.only(top: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Kelapa', style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w700,
                            ),),
                            Text('83,08%', style: TextStyle(
                              color: ColorPalette.greenColor,
                              fontSize: 12,
                              fontWeight: FontWeight.w700,
                            ),),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),

            //CARD KELIMA
            Container(
              margin: EdgeInsets.only(top: 0),
              child: Card(
                child: Container(
                  margin: EdgeInsets.all(16),
                  child: Column(
                    children: [
                      Container(
                        child: Row(
                          children: [
                            Text('Riwayat Penggunaan', style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w700,
                            ),),
                          ],
                        ),
                      ),
                      SizedBox(height: 8),
                      Container(
                        child: Column(
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  Icon(Icons.check_circle_sharp, size: 20, color: ColorPalette.greenColor),
                                  SizedBox(width: 8),
                                  Container(
                                    child: Card(
                                      color: ColorPalette.wallpaperColor,
                                      child: Container(
                                        width: 268,
                                        margin: EdgeInsets.all(8),
                                        child: Column(
                                          children: [
                                            Container(
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Container(
                                                      child: Column(
                                                        children: [
                                                          Text('Tanaman Jagung', style: TextStyle(
                                                            fontSize: 12,
                                                            fontWeight: FontWeight.w700
                                                          ),),
                                                          SizedBox(height: 8),
                                                          Text('September 2022', style: TextStyle(
                                                            fontSize: 12,
                                                            fontWeight: FontWeight.w400
                                                          ),),
                                                        ],
                                                      ),
                                                  ),
                                                  Text('99,08%', style: TextStyle(
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w700,
                                                    color: ColorPalette.greenColor,
                                                  ),)
                                                ],
                                              ),
                                            ),
                                            SizedBox(height: 8),
                                            Container(
                                              child: Row(
                                                children: [
                                                  Text('Keuntungan', style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight: FontWeight.w700
                                                  ),),
                                                  SizedBox(width: 70),
                                                  Text('Durasi Kontrak', style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight: FontWeight.w700
                                                  ),)
                                                ],
                                              ),
                                            ),
                                            SizedBox(height: 8),
                                            Container(
                                              child: Row(
                                                children: [
                                                  Text('Rp. 5.500.000', style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight: FontWeight.w400
                                                  ),),
                                                  SizedBox(width: 62),
                                                  Text('7 Bulan', style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight: FontWeight.w400
                                                  ),)
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),

                            Container(
                              child: Row(
                                children: [
                                  Icon(Icons.check_circle_sharp, size: 20, color: ColorPalette.orangeColor),
                                  SizedBox(width: 8),
                                  Container(
                                    child: Card(
                                      color: ColorPalette.wallpaperColor,
                                      child: Container(
                                        width: 268,
                                        margin: EdgeInsets.all(8),
                                        child: Column(
                                          children: [
                                            Container(
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Container(
                                                    child: Column(
                                                      children: [
                                                        Text('Kelapa Sawit', style: TextStyle(
                                                            fontSize: 12,
                                                            fontWeight: FontWeight.w700
                                                        ),),
                                                        SizedBox(height: 8),
                                                        Text('Januari 2022', style: TextStyle(
                                                            fontSize: 12,
                                                            fontWeight: FontWeight.w400
                                                        ),),
                                                      ],
                                                    ),
                                                  ),
                                                  Text('84,02%', style: TextStyle(
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w700,
                                                    color: ColorPalette.orangeColor,
                                                  ),)
                                                ],
                                              ),
                                            ),
                                            SizedBox(height: 8),
                                            Container(
                                              child: Row(
                                                children: [
                                                  Text('Keuntungan', style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight: FontWeight.w700
                                                  ),),
                                                  SizedBox(width: 70),
                                                  Text('Durasi Kontrak', style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight: FontWeight.w700
                                                  ),)
                                                ],
                                              ),
                                            ),
                                            SizedBox(height: 8),
                                            Container(
                                              child: Row(
                                                children: [
                                                  Text('Rp. 4.500.000', style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight: FontWeight.w400
                                                  ),),
                                                  SizedBox(width: 62),
                                                  Text('6 Bulan', style: TextStyle(
                                                      fontSize: 12,
                                                      fontWeight: FontWeight.w400
                                                  ),)
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),

            Container(
              margin: EdgeInsets.only(top: 16, bottom: 16, left: 8, right: 8),
              width: 328,
              height: 48,
              child: TextButton(
                style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(100),
                    ),
                    backgroundColor: ColorPalette.primaryColor),
                onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const KontrakLahan()));},
                child:
                Text(
                  "Ajukan Kontrak", style: TextStyle(
                  color: ColorPalette.secondaryColor,
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

