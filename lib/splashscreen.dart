import 'package:flutter/material.dart';
import 'dart:async';
import 'package:mitani_flutter/constants.dart';
import 'package:mitani_flutter/home.dart';
import 'package:mitani_flutter/welcomescreen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState(){
    super.initState();
    Timer(const Duration(seconds: 5),(){
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (_) => WelcomeScreen()));
    });
}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("images/logo_flutter.png",
                    height: 52,
                    width: 52,
                  ),
                  Text("Mitani", style: TextStyle(
                      color: ColorPalette.primaryColor,
                      fontSize: 32,
                      fontWeight: FontWeight.w800
                  ),),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            const CircularProgressIndicator.adaptive(
              valueColor: AlwaysStoppedAnimation<Color>(ColorPalette.primaryColor),
            ),
          ],
        ),
      ),
    );
  }
}
