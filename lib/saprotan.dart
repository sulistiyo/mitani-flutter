import 'package:flutter/material.dart';
import 'package:mitani_flutter/constants.dart';
import 'package:mitani_flutter/detail_saprotan.dart';
import 'package:mitani_flutter/notifikasi.dart';
import 'package:mitani_flutter/keranjang.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoding/geocoding.dart';

class Saprotan extends StatefulWidget {
  const Saprotan({Key? key}) : super(key: key);

  @override
  State<Saprotan> createState() => _SaprotanState();
}

class _SaprotanState extends State<Saprotan> {

  //SEARCH BOX
  TextEditingController? _textEditingController = TextEditingController();

  //GEOLOCATOR
  String location = 'Null, Press Button';
  String Address = 'search';

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    return await Geolocator.getCurrentPosition();
  }

  Future<void> GetAddressFromLatLong(Position position) async{
    List<Placemark> placemark = await placemarkFromCoordinates(position.latitude, position.longitude);
    print(placemark);
    Placemark place = placemark[0];

    Address = '${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';

    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          height: 36,
          margin: EdgeInsets.only(left: 0),
          decoration: BoxDecoration(color: Colors.white,
              borderRadius: BorderRadius.circular(42)),
          child: TextField(
            controller: _textEditingController,
            decoration: InputDecoration(
                border: InputBorder.none,
                errorBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                focusedBorder: InputBorder.none,
                contentPadding: EdgeInsets.all(2),
                prefixIcon: const Icon(Icons.search_sharp, color: ColorPalette.primaryColor, size: 20),
                hintText: 'Cari saprotan',
                hintStyle: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Colors.grey,
                )
            ),
          ),
        ),
        actions: [
          Container(
              height: 36,
              margin: EdgeInsets.only(right: 16),
              child: Row(
                children: [
                  CircleAvatar(
                    maxRadius: 20,
                    backgroundColor: Colors.white,
                    child: IconButton(
                      icon: Icon(Icons.notifications_sharp, size: 24),
                      onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Notifikasi()));},
                      color: ColorPalette.primaryColor,
                      splashColor: Colors.orange,
                      splashRadius: 2,
                      highlightColor: Colors.red,
                    ),
                  ),
                  SizedBox(width: 16),
                  CircleAvatar(
                    maxRadius: 20,
                    backgroundColor: Colors.white,
                    child: IconButton(
                      icon: Icon(Icons.shopping_bag, size: 24),
                      onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Keranjang()));},
                      color: ColorPalette.primaryColor,
                      splashColor: Colors.orange,
                      splashRadius: 2,
                      highlightColor: Colors.red,
                    ),
                  ),
                ],
              )
          )
        ],
        automaticallyImplyLeading: false,
        backgroundColor: ColorPalette.wallpaperColor,
      ),

      body: Container(
        color: ColorPalette.wallpaperColor,
        margin: EdgeInsets.only(top: 0),
        child: ListView(
          physics: ClampingScrollPhysics(),
          children: <Widget>[

            Container(
              margin: EdgeInsets.only(left: 16, right: 16, bottom: 24, top: 8),
              child: Column(
                children: [
                  Text('Titik Koordinat', style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                      color: Colors.black
                  ),),
                  Text(location, style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color: ColorPalette.greyColor
                  ),),
                  SizedBox(height: 8),
                  Text('Alamat Saat ini', style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                      color: Colors.black
                  ),),
                  Text('${Address}', style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color: ColorPalette.greyColor
                  ), textAlign: TextAlign.center,),
                  SizedBox(height: 8),
                  Container(
                    width: 132,
                    height: 36,
                    child: TextButton(
                      style: TextButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(100),
                          ),
                          backgroundColor: ColorPalette.primaryColor),
                      onPressed: () async {
                        Position position = await _determinePosition();
                        print(position.latitude);

                        location = 'Lat: ${position.latitude}, Long: ${position.longitude}';
                        GetAddressFromLatLong(position);
                        setState(() {

                        });


                      },
                      child: Text(
                        "Aktifkan Lokasi", style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                      ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

            Container(
              margin: EdgeInsets.only(left: 16, right: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Saprotan tersedia', style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                  ),
                  ),
                ],
              ),
            ),

            //ROW CARD PERTAMA
            Container(
              height: 254,
              width: 156,
              margin: EdgeInsets.only(left: 8, right: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Card(
                    elevation: 5,
                    margin: const EdgeInsets.all(8),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16)),
                    child: InkWell(
                      onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const DetailSaprotan()));},
                      splashColor: Colors.orange,
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          children:[
                            ClipRRect(
                              borderRadius: BorderRadius.circular(16),
                              child: Image.asset("images/img_traktor_kubota.jpg",
                                height: 101,
                                width: 156,
                                fit: BoxFit.cover,
                              ),
                            ),
                            SizedBox(height: 16),
                            Text("Traktor Merk Kubota",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                )),
                            SizedBox(height: 4),
                            Text("Sukamekar, Cirebon",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 10,
                                    fontWeight: FontWeight.w500
                                )),
                            SizedBox(height: 4),
                            Text("Rp. 24.000.000",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w700
                                )),
                            SizedBox(height: 16),
                            Container(
                              width: 132,
                              height: 36,
                              child: TextButton(
                                style: TextButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(100),
                                    ),
                                    backgroundColor: ColorPalette.primaryColor),
                                onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const DetailSaprotan()));},
                                child: Text(
                                  "+  Pesan", style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white,
                                ),
                                ),
                              ),
                            ),
                            SizedBox(height: 16),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Card(
                    elevation: 5,
                    margin: const EdgeInsets.all(8),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16)),
                    child: InkWell(
                      onTap: (){},
                      splashColor: Colors.orange,
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          children:[
                            ClipRRect(
                              borderRadius: BorderRadius.circular(16),
                              child: Image.asset("images/img_petani.jpg",
                                height: 101,
                                width: 156,
                                fit: BoxFit.cover,
                              ),
                            ),
                            SizedBox(height: 16),
                            Text("Jasa Tanam Padi",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                )),
                            SizedBox(height: 4),
                            Text("Sukamekar, Cirebon",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 10,
                                    fontWeight: FontWeight.w500
                                )),
                            SizedBox(height: 4),
                            Text("Rp. 4.000.000",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w700
                                )),
                            SizedBox(height: 16),
                            Container(
                              width: 132,
                              height: 36,
                              child: TextButton(
                                style: TextButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(100),
                                    ),
                                    backgroundColor: ColorPalette.primaryColor),
                                onPressed: (){},
                                child: Text(
                                  "+  Pesan", style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white,
                                ),
                                ),
                              ),
                            ),
                            SizedBox(height: 16),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

            //ROW CARD KEDUA
            Container(
              height: 254,
              width: 156,
              margin: EdgeInsets.only(left: 8, right: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Card(
                    elevation: 5,
                    margin: const EdgeInsets.all(8),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16)),
                    child: InkWell(
                      onTap: (){},
                      splashColor: Colors.orange,
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          children:[
                            ClipRRect(
                              borderRadius: BorderRadius.circular(16),
                              child: Image.asset("images/img_traktor_yanmar.jpg",
                                height: 101,
                                width: 156,
                                fit: BoxFit.cover,
                              ),
                            ),
                            SizedBox(height: 16),
                            Text("Traktor Merk Yanmar",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                )),
                            SizedBox(height: 4),
                            Text("Sukamekar, Cirebon",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 10,
                                    fontWeight: FontWeight.w500
                                )),
                            SizedBox(height: 4),
                            Text("Rp. 21.000.000",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w700
                                )),
                            SizedBox(height: 16),
                            Container(
                              width: 132,
                              height: 36,
                              child: TextButton(
                                style: TextButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(100),
                                    ),
                                    backgroundColor: ColorPalette.primaryColor),
                                onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const DetailSaprotan()));},
                                child: Text(
                                  "+  Pesan", style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white,
                                ),
                                ),
                              ),
                            ),
                            SizedBox(height: 16),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Card(
                    elevation: 5,
                    margin: const EdgeInsets.all(8),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16)),
                    child: InkWell(
                      onTap: (){},
                      splashColor: Colors.orange,
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          children:[
                            ClipRRect(
                              borderRadius: BorderRadius.circular(16),
                              child: Image.asset("images/img_angkut_padi.jpg",
                                height: 101,
                                width: 156,
                                fit: BoxFit.cover,
                              ),
                            ),
                            SizedBox(height: 16),
                            Text("Jasa Angkut Padi",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                )),
                            SizedBox(height: 4),
                            Text("Sukamekar, Cirebon",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 10,
                                    fontWeight: FontWeight.w500
                                )),
                            SizedBox(height: 4),
                            Text("Rp. 3.500.000",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w700
                                )),
                            SizedBox(height: 16),
                            Container(
                              width: 132,
                              height: 36,
                              child: TextButton(
                                style: TextButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(100),
                                    ),
                                    backgroundColor: ColorPalette.primaryColor),
                                onPressed: (){},
                                child: Text(
                                  "+  Pesan", style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white,
                                ),
                                ),
                              ),
                            ),
                            SizedBox(height: 16),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
