import 'package:flutter/material.dart';
import 'package:mitani_flutter/constants.dart';
import 'package:mitani_flutter/login.dart';
import 'package:text_form_field_wrapper/text_form_field_wrapper.dart';
import 'package:mitani_flutter/verifikasi_password.dart';

class ResetPassword extends StatefulWidget {
  const ResetPassword({Key? key}) : super(key: key);

  @override
  State<ResetPassword> createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(16),
        child: ListView(
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  _imageTitle(),
                  _titleDescription(),
                  _textField(),
                  _buildButton1(context),
                  _buildButton2(context),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

Widget _imageTitle(){
  return Container(
    margin: EdgeInsets.only(top: 24, bottom: 40),
    child: Image.asset('images/img_selfie.png',
      fit: BoxFit.cover,
      height: 247,
    ),
  );
}

Widget _titleDescription(){
  return Container(
    margin: EdgeInsets.only(top: 0),
    child: Column(
      children: [
        Row(
          children: [
            Text('Reset Password Kamu', style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w700,
            ),),
          ],
        ),
        SizedBox(height: 8),
        Text('Silakan masukkan email/username kamu untuk reset password kamu', style: TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w400,
          color: ColorPalette.greyColor
        ),)
      ],
    ),
  );
}

Widget _textField(){

  TextFormField formField = TextFormField(
    initialValue: '',
    decoration: const InputDecoration(border: InputBorder.none),
  );

  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(top: 32),
      ),
      SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Container(
              constraints: const BoxConstraints(maxWidth: 600),
              padding: const EdgeInsets.symmetric(
                horizontal: 0,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(
                    padding: EdgeInsets.only(left: 0),
                    child: Text('Email/Username', style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),),
                  ),
                  SizedBox(height: 8),
                  TextFormFieldWrapper(
                    formField: TextFormField(
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Masukkan alamat email kamu',
                        hintStyle: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: ColorPalette.hintColor,
                        ),
                      ),
                    ),
                    position: TextFormFieldPosition.alone,
                  ),
                  const  SizedBox(height: 109),
                ],
              ),
            ),
          ),
        ),
      ),
    ],
  );
}

Widget _buildButton1(BuildContext context){
  return Container(
    margin: EdgeInsets.only(top: 0, bottom: 0),
    width: 328,
    height: 48,
    child: TextButton(
      style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
          backgroundColor: ColorPalette.primaryColor),
      onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const VerfikasiPassword()));},
      child:
      Text(
        "Reset Password", style: TextStyle(
        color: Colors.white,
        fontSize: 14,
        fontWeight: FontWeight.w600,
      ),
      ),
    ),
  );
}

Widget _buildButton2(BuildContext context){
  return Container(
    margin: EdgeInsets.only(top: 8, bottom: 8),
    width: 328,
    height: 48,
    child: TextButton(
      style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
          backgroundColor: ColorPalette.secondaryColor),
      onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Login()));},
      child:
      Text(
        "Login Kembali", style: TextStyle(
        color: ColorPalette.primaryColor,
        fontSize: 14,
        fontWeight: FontWeight.w600,
      ),
      ),
    ),
  );
}