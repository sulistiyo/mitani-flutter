import 'package:flutter/material.dart';
import 'package:mitani_flutter/constants.dart';
import 'package:mitani_flutter/login.dart';
import 'package:mitani_flutter/registrasi.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  State<WelcomeScreen> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: ListView(
          children: [
            Column(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(0),
                  child: Image.asset('images/img_sawah_welcome.jpg',
                    fit: BoxFit.cover,
                    height: 492,
                  ),
                ),
                SizedBox(height: 45),
                Container(
                  margin: EdgeInsets.only(left: 32, right: 32),
                  child: Column(
                    children: [
                      Text("Memperkuat ketahanan pangan bagi Indonesia",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                        ),),
                      SizedBox(height: 8),
                      Text("Meningkatkan produktifitas lahan tidur",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                        ),),
                    ],
                  ),
                ),

                SizedBox(height: 115),

                Container(
                  margin: EdgeInsets.only(left: 16, right: 16),
                  child: Row(
                    children: [
                      Container(
                        width: 160,
                        height: 48,
                        child: TextButton(
                          style: TextButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(100),
                              ),
                              backgroundColor: ColorPalette.secondaryColor),
                          onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Login()));},
                          child:
                          Text(
                            "Skip", style: TextStyle(
                            color: ColorPalette.primaryColor,
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          ),
                          ),
                        ),
                      ),

                      const SizedBox(
                        width: 8,
                      ),

                      Container(
                        width: 160,
                        height: 48,
                        child: TextButton(
                          style: TextButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(100),
                              ),
                              backgroundColor: ColorPalette.primaryColor),
                          onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Registrasi()));},
                          child:
                          Text(
                            "Daftar Akun", style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 24),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
