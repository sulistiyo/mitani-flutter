import 'package:flutter/material.dart';
import 'package:mitani_flutter/constants.dart';
import 'package:mitani_flutter/syarat_mitani.dart';
import 'package:text_form_field_wrapper/text_form_field_wrapper.dart';
import 'package:mitani_flutter/login.dart';

class Registrasi extends StatefulWidget {
  const Registrasi({Key? key}) : super(key: key);

  @override
  State<Registrasi> createState() => _RegistrasiState();
}

class _RegistrasiState extends State<Registrasi> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container (
        color: ColorPalette.wallpaperColor,
        padding: EdgeInsets.all(16),
        child: ListView(
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  _titleDescription(),
                  _textField(),
                  _buildButton1(context),
                  _buildButton2(context),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget _titleDescription(){
  return Column(
    children: <Widget>[
      Container(
        margin: EdgeInsets.only(top: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("Daftar Pengolahan Lahan",
              style: TextStyle(
                color: Colors.black,
                fontSize: 16,
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        )
      ),
      SizedBox(height: 8),
      Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              "Pendaftaran ini, hanya unttuk mendaftarkan Akun Kamu. Selanjutnya akan ada kurasi dan konfirmasi kembali.",
              style: TextStyle(
                fontSize: 14,
                color: ColorPalette.greyColor,
                fontWeight: FontWeight.w400,
              ),
              textAlign: TextAlign.left,
            ),
          ],
        ),
      ),
    ],
  );
}

Widget _textField(){
  TextFormField formField = TextFormField(
    initialValue: '',
    decoration: const InputDecoration(border: InputBorder.none),
  );
  return Column(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(top: 24),
      ),
      SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Container(
              constraints: const BoxConstraints(maxWidth: 600),
              padding: const EdgeInsets.symmetric(
                horizontal: 0,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(
                    padding: EdgeInsets.only(left: 0),
                    child: Text('Nama', style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),),
                  ),
                  SizedBox(height: 8),
                  TextFormFieldWrapper(
                    formField: TextFormField(
                      decoration: const InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Masukkan nama kamu',
                        hintStyle: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: ColorPalette.hintColor,
                        ),
                      ),
                    ),
                    position: TextFormFieldPosition.alone,
                  ),
                  const  SizedBox(height: 24),
                  const Padding(
                    padding: EdgeInsets.only(left: 0),
                    child: Text('Username', style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),),
                  ),
                  SizedBox(height: 8),
                  TextFormFieldWrapper(
                    formField: TextFormField(
                      decoration: const InputDecoration(
                        contentPadding: const EdgeInsets.symmetric(vertical: 0),
                        border: InputBorder.none,
                        hintText: 'Masukkan username kamu',
                        hintStyle: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: ColorPalette.hintColor,
                        ),
                      ),
                    ),
                    position: TextFormFieldPosition.alone,
                  ),
                  SizedBox(height: 24),
                  const Padding(
                    padding: EdgeInsets.only(left: 0),
                    child: Text('E-mail', style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),),
                  ),
                  SizedBox(height: 8),
                  TextFormFieldWrapper(
                    formField: TextFormField(
                      decoration: const InputDecoration(
                        contentPadding: const EdgeInsets.symmetric(vertical: 0),
                        border: InputBorder.none,
                        hintText: 'Masukkan alamat email kamu',
                        hintStyle: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: ColorPalette.hintColor,
                        ),
                      ),
                    ),
                    position: TextFormFieldPosition.alone,
                  ),
                  SizedBox(height: 24),
                  const Padding(
                    padding: EdgeInsets.only(left: 0),
                    child: Text('Password', style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),),
                  ),
                  SizedBox(height: 8),
                  TextFormFieldWrapper(
                    formField: TextFormField(
                      obscureText: true,
                      decoration: const InputDecoration(
                        contentPadding: const EdgeInsets.symmetric(vertical: 0),
                        border: InputBorder.none,
                        hintText: 'Masukkan password kamu',
                        hintStyle: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: ColorPalette.hintColor,
                        ),
                      ),
                    ),
                    position: TextFormFieldPosition.alone,
                    suffix: const Icon(Icons.remove_red_eye, color: ColorPalette.hintColor,),
                  ),
                  SizedBox(height: 24),
                  const Padding(
                    padding: EdgeInsets.only(left: 0),
                    child: Text('Konfirmasi Password', style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500
                    ),),
                  ),
                  SizedBox(height: 8),
                  TextFormFieldWrapper(
                    formField: TextFormField(
                      obscureText: true,
                      decoration: const InputDecoration(
                        contentPadding: const EdgeInsets.symmetric(vertical: 0),
                        border: InputBorder.none,
                        hintText: 'Konfirmasi password kamu',
                        hintStyle: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: ColorPalette.hintColor,
                        ),
                      ),
                    ),
                    position: TextFormFieldPosition.alone,
                    suffix: const Icon(Icons.remove_red_eye, color: Colors.grey,),
                  ),
                  SizedBox(height: 40),
                ],
              ),
            ),
          ),
        ),
      ),
    ],
  );
}

Widget _buildButton1(BuildContext context){
  return Container(
    margin: EdgeInsets.only(top: 0, bottom: 0),
    width: 328,
    height: 48,
    child: TextButton(
      style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
          backgroundColor: ColorPalette.primaryColor),
      onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const SyaratMitani()));},
      child:
      Text(
        "Daftarkan Akun", style: TextStyle(
        color: Colors.white,
        fontSize: 14,
        fontWeight: FontWeight.w600,
      ),
      ),
    ),
  );
}

Widget _buildButton2(BuildContext context){
  return Container(
    margin: EdgeInsets.only(top: 8, bottom: 8),
    width: 328,
    height: 48,
    child: TextButton(
      style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
          backgroundColor: ColorPalette.secondaryColor),
      onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Login()));},
      child:
      Text(
        "Masuk", style: TextStyle(
        color: ColorPalette.primaryColor,
        fontSize: 14,
        fontWeight: FontWeight.w600,
      ),
      ),
    ),
  );
}
