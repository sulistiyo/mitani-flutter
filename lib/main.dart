import 'package:flutter/material.dart';
import 'package:mitani_flutter/splashscreen.dart';

void main() async {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'SplashScreen',
    home: SplashScreen(),
  ));
}
