import 'package:flutter/material.dart';
import 'package:mitani_flutter/constants.dart';
import 'package:mitani_flutter/home.dart';

class SyaratMitani extends StatefulWidget {
  const SyaratMitani({Key? key}) : super(key: key);

  @override
  State<SyaratMitani> createState() => _SyaratMitaniState();
}

class _SyaratMitaniState extends State<SyaratMitani> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: ColorPalette.primaryColor,
          onPressed: () => Navigator.of(context).pop(),
        ),
        backgroundColor: ColorPalette.wallpaperColor,
      ),
      body: Container(
        color: ColorPalette.wallpaperColor,
        padding: EdgeInsets.all(16),
        child: ListView(
          physics: ClampingScrollPhysics(),
          children: [
            Text('Syarat dan Ketentuan Berlaku', style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w700,
            ),),

            SizedBox(height: 16),

            Container(
              padding: EdgeInsets.all(16),
              color: Colors.white,
              height: 440,
              width: 328,
              child: ListView(
                children: [
                  Text('Syarat dan Ketentuan Ketentuan Penggunaan MOHON ANDA MEMERIKSA SYARAT DAN KETENTUAN DAN KEBIJAKAN PRIVASI MITANI DENGAN SEKSAMA SEBELUM MEMBUAT AKUN DAN MENGGUNAKAN LAYANAN MITANI UNTUK PERTAMA KALI. Dengan mengakses dan/atau menggunakan Platform MiTani, Anda setuju bahwa Anda telah membaca, memahami dengan seksama dan menyetujui untuk terikat dengan Ketentuan Penggunaan ini ("Ketentuan Penggunaan"). Ketentuan Penggunaan ini merupakan suatu perjanjian sah antara Anda dan MiTani (Perum Perhutani) sehubungan dengan kunjungan, akses dan penggunaan Anda pada Layanan dan Platform MiTani Mobile.JIKA ANDA TIDAK SETUJU, BAIK SEBAGIAN MAUPUN SELURUHNYA, DARI KETENTUAN PENGGUNAAN INI, MAKA ANDA TIDAK DAPAT MENGAKSES DAN MENGGUNAKAN LAYANAN PLATFORM.',
                  textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 44),
            CheckboxListTile(
                value: true,
                onChanged: (value) {},
              controlAffinity: ListTileControlAffinity.leading,
              title: Text('Saya telah membaca dan menyetujui syarat dan ketentuan diatas', style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400
              ),),
            ),

            Container(
              margin: EdgeInsets.only(top: 32, bottom: 8),
              width: 328,
              height: 48,
              child: TextButton(
                style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(100),
                    ),
                    backgroundColor: ColorPalette.primaryColor),
                onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Home()));},
                child:
                Text(
                  "Selanjutnya", style: TextStyle(
                  color: ColorPalette.secondaryColor,
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                ),
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
