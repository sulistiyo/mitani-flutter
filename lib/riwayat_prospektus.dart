import 'package:flutter/material.dart';
import 'package:mitani_flutter/constants.dart';
import 'package:mitani_flutter/detail_lahan.dart';
import 'package:mitani_flutter/home.dart';
import 'package:mitani_flutter/portofolio.dart';
import 'package:mitani_flutter/riwayat_progres.dart';

class RiwayatProspektus extends StatefulWidget {
  const RiwayatProspektus({Key? key}) : super(key: key);

  @override
  State<RiwayatProspektus> createState() => _RiwayatProspektusState();
}

class _RiwayatProspektusState extends State<RiwayatProspektus> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: ColorPalette.primaryColor,
          onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Home()));},
        ),
        title: Text(
          'Riwayat', style: TextStyle(
          color: Colors.black,
        ),
        ),
        backgroundColor: ColorPalette.wallpaperColor,
      ),
      body: Container(
        color: ColorPalette.wallpaperColor,
        padding: EdgeInsets.all(16),
        child: ListView(
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  _tabBar(context),
                  _cardImage(context),
                  _cardDescription(),
                  _cardPendamping(),
                  _buildButton(context),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget _tabBar(BuildContext context){
  return Container(
    padding: EdgeInsets.only(bottom: 16),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          height: 36,
          width: 95,
          child: TextButton(
            style: TextButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(100),
                ),
                backgroundColor: Colors.white),
            onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const RiwayatProgres()));},
            child: Text(
              "Progres", style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w600,
              color: ColorPalette.primaryColor,
            ),
            ),
          ),
        ),
        SizedBox(width: 8),
        Container(
          height: 36,
          width: 95,
          child: TextButton(
            style: TextButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(100),
                ),
                backgroundColor: ColorPalette.secondaryColor),
            onPressed: (){},
            child: Text(
              "Prospektus", style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w600,
              color: ColorPalette.primaryColor,
            ),
            ),
          ),
        ),
      ],
    ),
  );
}

Widget _cardImage (BuildContext context){
  return Container(
    height: 100,
    width: 328,
    child: Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12)),
      child: InkWell(
        onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const DetailLahan()));},
        splashColor: Colors.orange,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(padding: EdgeInsets.only(left: 16)),
              ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: Image.asset("images/img_sawah1.jpg",
                  height: 60,
                  width: 60,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(width: 16),
              Container(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Lahan Baru Bagus",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          )),
                      SizedBox(height: 4),
                      Text("Kartonyono, Ngawi",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w300
                          )),
                      SizedBox(height: 4),
                      Text("200 x 100 m",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          )),
                    ],
                  ),
                ),
              ),
              SizedBox(width: 80),
              Icon(Icons.arrow_forward_ios, size: 15, color: Colors.grey),
            ],
          ),
        ),
      ),
    ),
  );
}

Widget _cardDescription(){
  return Container(
    margin: EdgeInsets.only(top: 16),
    color: Colors.white,
    padding: EdgeInsets.all(16),
    child: Column(
      children: [
        Row(
          children: [
            Text('Kebutuhan Dana', style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w700,
            ),),
          ],
        ),
        SizedBox(height: 8),
        Row(
          children: [
            Text('Rp. 200.000.000', style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w400,
              color: Colors.grey
            ),),
          ],
        ),
        SizedBox(height: 16),
        Row(
          children: [
            Text('Deskripsi', style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w700,
            ),),
          ],
        ),
        Text('I am Sarah. My full name is Sarah Lau Smith. I am a second grade student of Junior High School. I am 14 years old. My birth day is on July 5. In my family, I am the second child. I have an older brother named Nathan and one younger sister named Grace. My mother is from China and my father is from Australia. We live in Indonesia.', style: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w400,
            color: Colors.grey
        ),),
        SizedBox(height: 16),
        Row(
          children: [
            Text('Dokumen Prospektus', style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w700,
            ),),
          ],
        ),
        SizedBox(height: 8),
        Row(
          children: [
            Icon(Icons.description_sharp, size: 24, color: ColorPalette.primaryColor,),
            SizedBox(width: 8),
            Text('Doc_progress.pdf', style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w400,
              color: ColorPalette.primaryColor
            ),),
          ],
        )
      ],
    ),
  );
}

Widget _cardPendamping(){
  return Container(
    margin: EdgeInsets.only(top: 16),
    color: Colors.white,
    padding: EdgeInsets.all(16),
    child: Column(
      children: [
        Row(
          children: [
            Text('Catatan Pendamping', style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w700,
            ),),
          ],
        ),
        SizedBox(height: 16),
        Row(
          children: [
            Text('1 Desember 2022', style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Colors.grey
            ),),
          ],
        ),
        SizedBox(height: 8),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Jayadi', style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w500,
            ),),
            Text('Approve', style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w400,
              color: ColorPalette.greenColor,
            ),)
          ],
        ),
        SizedBox(height: 8),
        Text('In the future, I want to be a veterinarian. I want to help animals to survive so they can live longer. I like watching their unique behaviors.  They are all beautiful creatures. I love animals.', style: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w400,
            color: Colors.grey
        ),),


        SizedBox(height: 16),
        Row(
          children: [
            Text('10 Desember 2022', style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Colors.grey
            ),),
          ],
        ),
        SizedBox(height: 8),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Jayadi', style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w500,
            ),),
            Text('Revisi Dokumen', style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w400,
              color: ColorPalette.orangeColor,
            ),)
          ],
        ),
        SizedBox(height: 8),
        Text('She is never afraid of danger or being injured. She always says that extreme sports are a part of her life. She is alive and feels joy of life while doing the extreme sport.', style: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w400,
            color: Colors.grey
        ),),
      ],
    ),
  );
}

Widget _buildButton(BuildContext context){
  return Container(
    margin: EdgeInsets.only(top: 117, bottom: 8),
    width: 328,
    height: 48,
    child: TextButton(
      style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
          backgroundColor: ColorPalette.primaryColor),
      onPressed: (){},
      child:
      Text(
        "Ajukan Ke Investor", style: TextStyle(
        color: Colors.white,
        fontSize: 14,
        fontWeight: FontWeight.w600,
      ),
      ),
    ),
  );
}
