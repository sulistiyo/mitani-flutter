import 'package:flutter/material.dart';
import 'package:mitani_flutter/constants.dart';
import 'package:mitani_flutter/detail_lahan.dart';
import 'package:mitani_flutter/home.dart';
import 'package:mitani_flutter/portofolio.dart';
import 'package:mitani_flutter/riwayat_prospektus.dart';

class RiwayatProgres extends StatefulWidget {
  const RiwayatProgres({Key? key}) : super(key: key);

  @override
  State<RiwayatProgres> createState() => _RiwayatProgresState();
}

class _RiwayatProgresState extends State<RiwayatProgres> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: ColorPalette.primaryColor,
          onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const Home()));},
        ),
        title: Text(
          'Riwayat', style: TextStyle(
          color: Colors.black,
        ),
        ),
        backgroundColor: ColorPalette.wallpaperColor,
      ),
      body: Container(
        color: ColorPalette.wallpaperColor,
        padding: EdgeInsets.all(16),
        child: ListView(
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  _tabBar(context),
                  _cardImage(context),
                  _titleDescription1(),
                  _historyList(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget _tabBar(BuildContext context){
  return Container(
    padding: EdgeInsets.only(bottom: 16),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          height: 36,
          width: 95,
          child: TextButton(
            style: TextButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(100),
                ),
                backgroundColor: ColorPalette.secondaryColor),
            onPressed: (){},
            child: Text(
              "Progres", style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w600,
              color: ColorPalette.primaryColor,
            ),
            ),
          ),
        ),
        SizedBox(width: 8),
        Container(
          height: 36,
          width: 95,
          child: TextButton(
            style: TextButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(100),
                ),
                backgroundColor: Colors.white),
            onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const RiwayatProspektus()));},
            child: Text(
              "Prospektus", style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w600,
              color: ColorPalette.primaryColor,
            ),
            ),
          ),
        ),
      ],
    ),
  );
}

Widget _cardImage (BuildContext context){
  return Container(
    height: 100,
    width: 328,
    child: Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12)),
      child: InkWell(
        onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const DetailLahan()));},
        splashColor: Colors.orange,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(padding: EdgeInsets.only(left: 16)),
              ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: Image.asset("images/img_sawah1.jpg",
                  height: 60,
                  width: 60,
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(width: 16),
              Container(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Lahan Baru Bagus",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          )),
                      SizedBox(height: 4),
                      Text("Kartonyono, Ngawi",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w300
                          )),
                      SizedBox(height: 4),
                      Text("200 x 100 m",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          )),
                    ],
                  ),
                ),
              ),
              SizedBox(width: 80),
              Icon(Icons.arrow_forward_ios, size: 15, color: Colors.grey),
            ],
          ),
        ),
      ),
    ),
  );
}

Widget _titleDescription1(){
  return Container(
    margin: EdgeInsets.only(right: 16, top: 16, bottom: 16),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text('Riwayat Progres Pengelolaan', style: TextStyle(
          color: Colors.black,
          fontSize: 14,
          fontWeight: FontWeight.w700,
        ),
        ),
      ],
    ),
  );
}

Widget _historyList(){
  return Column(
    children: [
      Container(
        height: 40,
        width: 328,
        child: Card(
          elevation: 5,
          margin: const EdgeInsets.only(bottom: 0),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8)),
          child: InkWell(
            onTap: (){},
            splashColor: Colors.orange,
            child: Container(
              margin: EdgeInsets.only(left: 8, right: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('20 Januari 2023', style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w700
                  ),),
                  Icon(Icons.expand_more_sharp, size: 20),
                ],
              ),
            ),
          ),
        ),
      ),
      SizedBox(height: 8),
      Container(
        height: 40,
        width: 328,
        child: Card(
          elevation: 5,
          margin: const EdgeInsets.only(top: 0),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8)),
          child: InkWell(
            onTap: (){},
            splashColor: Colors.orange,
            child: Container(
              margin: EdgeInsets.only(left: 8, right: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('10 Januari 2023', style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w700
                  ),),
                  Icon(Icons.expand_more_sharp, size: 20),
                ],
              ),
            ),
          ),
        ),
      ),
      SizedBox(height: 8),
      Container(
        height: 40,
        width: 328,
        child: Card(
          elevation: 5,
          margin: const EdgeInsets.only(top: 0),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8)),
          child: InkWell(
            onTap: (){},
            splashColor: Colors.orange,
            child: Container(
              margin: EdgeInsets.only(left: 8, right: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('5 Januari 2023', style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w700
                  ),),
                  Icon(Icons.expand_more_sharp, size: 20),
                ],
              ),
            ),
          ),
        ),
      ),
      SizedBox(height: 8),
      Container(
        height: 40,
        width: 328,
        child: Card(
          elevation: 5,
          margin: const EdgeInsets.only(top: 0),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8)),
          child: InkWell(
            onTap: (){},
            splashColor: Colors.orange,
            child: Container(
              margin: EdgeInsets.only(left: 8, right: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('1 Januari 2023', style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w700
                  ),),
                  Icon(Icons.expand_more_sharp, size: 20),
                ],
              ),
            ),
          ),
        ),
      ),
    ],
  );
}
